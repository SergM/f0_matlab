function win = LPC_Filter_v2(Window, Coeff)
    win_Size = size(Window,1);
    win = zeros(win_Size,1);
    Order = size(Coeff,1);
    for i = 1+Order:win_Size-Order
        accum = 0;
        for j=1:Order
            accum = accum - Coeff(j) * Window(i-j+1);
        end
        win(i) = accum;
    end
end