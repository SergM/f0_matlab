% ��� ��������� �� start �� end
% ���� ��������� ���� ������� ������������� ����������������
function tracer_1(start_check_pntr, end_check_pntr, direction,HNR_slope,max_HNR,betta,FR_slope,FR_sigm_width,gamma,PWR_slope,zcross_PWR,delta,freqWidth)

    global voice;       % ������ �������� VAD    
%    global HNR;         % ������ �������� HNR
    global F0_Cand;     % ������ ������ � ����������
    global F0;          % ������ ������ ���
    
    
%     if direction == -1
%         F0(start_check_pntr:direction:end_check_pntr) = 700;
%     else
%         F0(start_check_pntr) = 600;
%         F0(start_check_pntr+1:end_check_pntr) = 500;
%     end
%     return;

    
    % ��������� ���������� ������������ � ���� F0
     VAD_cnt = (end_check_pntr-start_check_pntr)*direction+1;
%     
%     HNR_slope = 1; % �������� �������� HNR
%     max_HNR = 5;  % ����� �������� �������� ����� 0
% %     HNR_slope = 0.7; % �������� �������� HNR
% %     max_HNR = 6;  % ����� �������� �������� ����� 0
%     
%     betta = 300; % ����������� ����� HNR ������������ ����
%     
%     FR_slope = 15; % �������� �������� FREQUENCY
%     FR_sigm_width = 5;% ������ ����� �������
%  
%     gamma = 400; % ����������� ����� ��������� ������������ ����
%     
%     PWR_slope = 3; % �������� �������� PWR
%     zcross_PWR = 0.1; % ����� �������� �������� ����� 0
% %     PWR_slope = 8; % �������� �������� PWR
% %     zcross_PWR = 0.2; % ����� �������� �������� ����� 0
% 
%     delta = 50; % ����������� ����� PWR ������������ ����
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ������ ����� ��������� ������� ����� ��������� ��� �����
    
    % �����������   ������� - ���
    %     1         ����� ��������� �� �������� ���� �������
    %     2         ����� ��������� � �������� ���� �������
    %     3         ����� ���� (�������), ������� ������� ������� �������
    Cand_num = size(F0_Cand,2);
    Graph = ones(Cand_num,Cand_num,VAD_cnt) * Inf;
    
    % ���� ����������� �� ������ � start_check_pntr
    % ������ ����� �������� ������ ������� � ��� ������������ F0 ��� (���
    % ������� ���������) ������������ ���� �� ����������� �������� �������
    % ����
    
    for stage = 1:VAD_cnt % ��� ���� ����� (�������)
        
        cand_from_ind = end_check_pntr - (stage - 1) * direction;
        cand_to_ind   = end_check_pntr - (stage - 1 + 1 ) * direction;
        max_PWR = max(max(F0_Cand(cand_to_ind-3:cand_to_ind+3,:,6)));
        %max_PWR = 1;
        
        for cand_from = 1:Cand_num
            
            freq_from = F0_Cand(cand_from_ind,cand_from,2);
            if (freq_from ~= 0)
                for cand_to = 1:Cand_num

                    
                    % ��������� �������� ������������ �� ���� ����� HNR �
                    % ���������, �� �������� ���� ����������
                    % � ������ ��������� �� �������
                    freq_to   = F0_Cand(cand_to_ind,cand_to,2);
                    if (freq_to ~= 0)


                        % ����������, ����� ��� ������ �������� (direction == -1)
                        if (direction == -1) && (stage == VAD_cnt)
                            % ����������� ����
                            cost = 0;
                        else
                                fr_dist = abs(freq_from - freq_to);
                                if freq_from == 0 
                                    fprintf('������� �� ����  cand_to_ind = %d  cand_from_ind = %d\n',cand_to_ind,cand_from_ind);
                                end
                                %fr_dist_rate = fr_dist/freq_from;
                                
                                fr_dist_rate = 1-2*(1/(1+exp(FR_slope*fr_dist/freqWidth-FR_sigm_width)));
                                
                                %fr_dist_rate = fr_score(freq_from, freq_to);

                                % �������� ����� �������� �� ���������
                                if direction == -1
                                    cand_ind = cand_to_ind;
                                else
                                    cand_ind = cand_from_ind;
                                end
                                
                                HNR_to = 1-2*(1/(1+exp(HNR_slope*(max_HNR-F0_Cand(cand_ind,cand_to,1)))));
                                PWR_to = 1-2*(1/(1+exp(PWR_slope*(zcross_PWR-F0_Cand(cand_ind,cand_to,6)/max_PWR))));

                                cost = HNR_to * betta + fr_dist_rate * gamma + PWR_to * delta;
                        end

                        Graph(cand_from,cand_to,stage) = cost;
                    end
                end
            end
        end
    end
    
    
    % ������������ ����������������
    
    % ������ ��� ���� ������ �����
    % �������:
    % ���������� ������� �������� ��������� ���� ������� � ������� ��
    % �������: (stage-1)*Cand_num + candidat 
    % ��� cpp: (stage*Cand_num + candidat)
    
    
    % �������� ���� �� ����� � ������
    
    
    % 1, 2 - ���������� ������� � ����� ������� ����� (�������� ����������) (stage,candidat)
    % 3 ����������� ��������� ��������� �� ������� �� �����
    vertex_num = Cand_num*(VAD_cnt);
    vertexes = ones(vertex_num,3) * Inf;
    
    for stage = VAD_cnt:-1:1
        % ���� ��������� �������� �� ���������� ������� � �������
        for cand_from = 1:Cand_num
            vertex_path_cost = Inf;
            for cand_to = 1:Cand_num
                c = Graph(cand_from,cand_to,stage);
                if c~= Inf
                    % ���� ��� ������� ������ ������� �����
                    if (stage == VAD_cnt)
                        path_cost = 0;
                    else
                        % ���� vertex � ������������ cand_to,stage+1
                        path_cost = vertexes((stage)*Cand_num + cand_to,3);
                    end
                    % ���������� ��� ��������� � ����������� � ������� vertexes
                    path_cost = path_cost + c;
                    if path_cost < vertex_path_cost
                        vertex_path_cost = path_cost;
                        vertex_back_stage = stage;
                        vertex_back_cand = cand_to;
                    end
                end
            end
            % ���������� ��������� ����� � vertexes
            if vertex_path_cost ~= Inf
                vertexes((vertex_back_stage-1)*Cand_num + cand_from,1) = vertex_back_stage;
                vertexes((vertex_back_stage-1)*Cand_num + cand_from,2) = vertex_back_cand;
                vertexes((vertex_back_stage-1)*Cand_num + cand_from,3) = vertex_path_cost;
            end
        end
        % �������� ����������� ��������� � ���������� �� � 
    end
    
    % ��� ������� ��������� ��������� ���� ����� ������ ������� ��
    % ����������� ���� � �����
    

    % �������� ���� �� ������ � �����

    
    
    

    
    % ��� ���������� ����
    [~,first_best_cand] = min(vertexes(1:Cand_num,3));
    best_cand = first_best_cand;
% 
%     if direction == 1
         F0(end_check_pntr)=F0_Cand(end_check_pntr,best_cand,2);
%     else
%         F0(start_check_pntr)=F0_Cand(start_check_pntr,best_cand,2);
%     end
    
    stage = 2;
    for i = end_check_pntr-direction:(-1)*direction:start_check_pntr
        best_cand = vertexes((stage-1-1)* Cand_num + best_cand,2);
        %fprintf('best_cand = %d  i = %d  stage = %d\n',best_cand,i,stage);
        if F0_Cand(i,best_cand,1) < 1
            voice(i) = 3;
        elseif F0_Cand(i,best_cand,1) < 2.5
            voice(i) = 2;
        end
        F0(i)=F0_Cand(i,best_cand,2);
        stage = stage + 1;
    end

    % ���� ��� �� ������ ���� ���������� ����� �� �����, �� 
    % ����� ������� ���� ������������
    
%     determ_point = start_check_pntr;
%     if direction == 1
%         determ_point = end_check_pntr;
%     end

    
% ���� �������� ����� �������� �� ����� �������� ���������, �� ���� �������
% ������ ��� ������� ������������� �����

%     if F0_Cand(determ_point,best_cand,1) > 7
%         if direction == 1
%             last_best_cand = first_best_cand;
%         else
%             last_best_cand = best_cand;
%         end
%        fprintf('�������� %f �� �� %f � point = %d  sart = %d   stop = %d\n',F0_Cand(determ_point,last_best_cand,2),determ_point*256/16000,determ_point,start_check_pntr,end_check_pntr);
%         for i = 1:Cand_num
%             if i ~= last_best_cand
%                 F0_Cand(determ_point,i,1) = 0;
%                 F0_Cand(determ_point,i,2) = 0;
%                 F0_Cand(determ_point,i,6) = 0;
%             end
%         end
%     end
    
end