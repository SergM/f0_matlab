% ��������� ���������� �� ������ ��������� ����.
% �� �����:
%    - ����� �������� ���� ���������,
%    - ������� �������������,
%    - ����������� ���,
%    - ������������ ���,
%    - ���������� �������, �� ������� ��������� ���� ���������,
%    - ��������� �� ���������� ������ �������������,
%    - ��������� �� ���������� ������ ������� �����,
%    - ��������� �� ���������� ������ ��� ���������� ���������� F0,
%    - ��������� �� ���������� ������ ��� ���������� �������� HNR (Harmonic to Nioze Rate),
%    - ��������� �� ���������� ������ ��� ���������� NSI (Noize Strength Index),
%    - ��������� �� ���������� ���������� ��� ����������� ������
%    - ��������� �� ���������� ������ ��� ���������������� VAD,


function F0_cand9(pntr, Fd, N_candidates)%, flog)

    first_ind = 1;  % 0 - ��� cpp, 1 - ��� matlab

    global Sp F0 F0_Cand HNR NSI Vol;
    
    global Debug;
    
    %   Sp - ����������� ������������������� ������ �� ����
    %   Fd - ������� �������������
    %   fmin - ����������� ����������� ������� ��������� ���� (���)
    %   fmax - ������������ ����������� ���
    %   N_candidates - ���������� ����������� ���������� ���

    % ���������� ���������� (�� ����� ������� � ��������)
    
    bad_HNR = 0.8;  % ������ ������� HNR ��� ������ ����������
    
    good_HNR = 3;             % ������� ����� �����������
    not_too_bad_HNR = 1.5;      % ������ ����� �����������
    
    good_SNR = 10; % ������� ����� �����������
    bad_SNR = 3;  % ������ ����� ����������� (��. ���� ��������!)

    fmin = 67;  % ����������� ����������� �������
    fmax = 450;  % ������������ ����������� �������

    check_harm = 7; % ���������� �������� ��� ������� HNR
    low_fr_noize = 250; %������� ���� ������� ��������� �� ��������������� ��� ���������� HNR
    
    N = size(Sp,1);   % ���������� ����� � �������������� �������
    bin_f = Fd/2/N;   % ������� �� ���� ���
    
    % ������ � ��������� ���������� � ���
    Candidates = zeros(N_candidates,6);% 1-� - HNR
                                       % 2-� - ������� ��������� ���� (���)
                                       % 3-� - ���
                                       % 4-� - ��� � ������
                                       % 5-� - 
                                       % 6-� - ������� ������ ��������� ���
    
    
    % ��� �������     
    ccc = []; % ���������� ������
    fff = []; % ���������� ������



    % ������� ����� ������������� ���� ������� � ��������� �� 1500��
    [max_pwr,max_pwr_bin] = max(Sp((round(150/bin_f)+first_ind-1):(round(1500/bin_f)+first_ind-1),pntr+first_ind-1));
    
    if max_pwr > 0
        Dithering_factor = max_pwr/1000;
    else
        Dithering_factor = 1E-6;
    end
    
    f = fmin;
    
    % �������� ���������� �� ��� � ��������� ���������� �������� � ���
    while f<fmax
         
        % ��������� ��� ���� ��� ����������� ���������-��� �� ������
        % check_harm ����������
        SpHarmonics = 0;

        % ���������� ������� ��� ���� � ������� � ������� ����
        
        % ������� ������ ��� ��������� �������
        first_bin = round(f/bin_f)+1;
        % ������� ��������� � ������������� ���� ������� ��� � ���������
        rate = round(max_pwr_bin/first_bin)+1;
        
        %  �������� ������ ����������� ��������� ���, ��� �� ������������
        %  ��� ������� �� �������� ��������� ����������� �������� ���� ���
        %  ��������
        
        mid_check_band = round((check_harm+1)/2);
        
        if rate >= mid_check_band
            start_c = rate - mid_check_band + 1;
        else
            start_c = 1;
        end
        
        % ��������� ��������� ��������� ���� ��������� ������� ��� ���������� ����
        % ����������� ����
        start_c = start_c + fix((low_fr_noize/(start_c*f)));
        
        % ���������� check_harm �������� ������� �� start_c
        for c = start_c:(start_c + check_harm - 1)
            current_bin = round(f*c/bin_f)+1;
            if current_bin > 3%6
                SpHarmonics = SpHarmonics + Sp(current_bin+first_ind-1,pntr+first_ind-1)+...
                    Sp(current_bin+1+first_ind-1,pntr+first_ind-1)+Sp(current_bin-1+first_ind-1,pntr+first_ind-1);
            end
        end

        % ��� - ��� �� ��������� (��������� �� ���������� �����)
        start_noise = round(f*(start_c-1)/bin_f)+1+2;
        end_noise   = round(f*(start_c-1 + check_harm)/bin_f)+1+1;
        
        if pntr == 3018
            fprintf('%d\t%d\t%.10e\t%d\t%d\n',max_pwr_bin,rate,f,start_noise,end_noise);
            
        end
        
        SpNoize = (sum(Sp(start_noise+first_ind-1:end_noise+first_ind-1,pntr+first_ind-1))-SpHarmonics)/...
            ((end_noise-start_noise+1)-3*check_harm);        

        % ������ ���� ��������� �� ���������� �����
        SpHarmonics = SpHarmonics/(3*check_harm);
        SpVocalHNR = ((SpHarmonics+Dithering_factor)/(SpNoize+Dithering_factor));
        
        
        % ��� �������
        fff = [fff f];
        ccc = [ccc SpVocalHNR];

        insert_pos = 0;
        lowest_level = 10^100;
        if (SpVocalHNR > bad_HNR)
            for i = 1:N_candidates
                % ���������� ���� ����������
                % ���� ������� ������, �� �������� ������� � SNR
                if abs(f - Candidates(i,2)) <= bin_f*2
                    % ���� ������� ����, �� ��������
                    if (SpVocalHNR >= Candidates(i,1))
                        Candidates(i+first_ind-1,1+first_ind-1) = SpVocalHNR;
                        Candidates(i+first_ind-1,2+first_ind-1) = f;
                        Candidates(i+first_ind-1,4+first_ind-1) = SpNoize;
                        Candidates(i+first_ind-1,6+first_ind-1) = Sp(first_bin+first_ind-1,pntr+first_ind-1);
                        insert_pos = 0;
                        break
                    end
                else
                    if (SpVocalHNR >= Candidates(i+first_ind-1,1+first_ind-1))
                        % ���������� ����������� ������� ��������� � ���
                        % �������
                        if Candidates(i+first_ind-1,1+first_ind-1) < lowest_level
                            lowest_level = Candidates(i+first_ind-1,1+first_ind-1);
                            insert_pos = i;
                            if Candidates(i+first_ind-1,1+first_ind-1) == 0
                                % ���� ������ ���, �� ����� ������ �� ����������
                                break
                            end
                        end
                    end
                end
            end    
            % ���� ����� ����� ��� � �� ��������
            if insert_pos ~= 0
                % �� �� � �� ���� ���������, ���� ����� ���� ����� �������
                % �����
                if insert_pos > 1
                    prev_cand_freq = Candidates(insert_pos-1+first_ind-1,2+first_ind-1);
                else
                    prev_cand_freq = 0;
                end
                if (abs(f - prev_cand_freq) > bin_f*2)
                    Candidates(insert_pos+first_ind-1,1+first_ind-1) = SpVocalHNR;
                    Candidates(insert_pos+first_ind-1,2+first_ind-1) = f;
                    Candidates(insert_pos+first_ind-1,4+first_ind-1) = SpNoize;
                    Candidates(insert_pos+first_ind-1,6+first_ind-1) = Sp(first_bin+first_ind-1,pntr+first_ind-1);
                end
            end
        end
        f=f+bin_f/check_harm;
    end

  % ��� �������
    if pntr == 3018
        figure('Name','Debug');
        subplot(2,1,1);
        plot(fff,ccc,'LineStyle','-','LineWidth',2);
        hold on;
        plot(Candidates(:,2),Candidates(:,1),'LineStyle','none','LineWidth',2,'Marker','o');
        plot(Candidates(:,2),Candidates(:,6).*100,'LineStyle','none','LineWidth',2,'Marker','x');
        xlim([0 500]);
        subplot(2,1,2);
        bar(0:bin_f:Fd/2-bin_f,Sp(:,pntr));
        xlim([0 2000]);
    end
    
    % ��������� ������� ���� ��� ������� ����, ��� ����������� ��������
    % ���� �� ���������� �� ������ 0
    %
    % ���� ���� �������� � �������� ���� bin_f*10 � ��� ���� � ������� HNR,
    % �� ������ ����� ������� � ���� ����� ��������� ������� �������� ����
    
    % �� ���������, ��� ��� �����-�� ����������� �������� �� �������...
    % �������� ������� �������� �� 10 ��.
    
    current_noize = 1;
    
    for i=11:10:100
        avg_spectr = sum(Sp(i:i+9+first_ind-1,pntr+first_ind-1))/10;
        if current_noize > avg_spectr
            current_noize = avg_spectr;
        end
    end
    
    % ���������� ����������
    for i=1:N_candidates
        corrected_cand_noize = Candidates(i+first_ind-1,4+first_ind-1);
        % ���� HNR ��������� �� ������, ��
%         if Candidates(i+first_ind-1,1+first_ind-1) > not_too_bad_HNR
%             if Candidates(i+first_ind-1,2+first_ind-1) < bin_f*10
%                 if pntr>1
%                     current_noize = NSI(pntr-1);
%                 end
%             end
            if current_noize > corrected_cand_noize
                current_noize = corrected_cand_noize;
            end
%         end
    end
    
    % ��������� ���������� c ��������� �������� �� ��������! HNR
    not_nul_fr_ind = find(Candidates(:,1+first_ind-1));
    cand_to_sort = zeros(length(not_nul_fr_ind),6);
    cand_to_sort(:,:) = Candidates(not_nul_fr_ind,:);
    Candidates(:,:) = [sortrows(cand_to_sort,1+first_ind-1,'descend'); zeros(N_candidates - length(not_nul_fr_ind),6)];
    
    if Candidates(1,1+first_ind-1) > 7
        Candidates(3:end,:) = 0;
    elseif Candidates(1,1+first_ind-1) == 0
        if pntr>1
            Candidates(:,:) = F0_Cand(pntr-1,:,:);
        end
    else
        Candidates(7:end,:) = 0;
    end
    
%     fprintf(flog,'pntr = %d\nCandidates:\n',pntr);
%     for i=1:8
%         fprintf(flog,'%d\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\n',i,Candidates(i,:));
%     end
%     fprintf(flog,' \n',pntr);
    
    HNR_to_store = max(Candidates(:,1+first_ind-1));
    % �������� ������ ����������� HNR
    HNR(pntr+first_ind-1) = HNR_to_store;
    
    % �������� ������ ��� ������� �������� ���� (�������� �� 5 ����)
    if pntr>=5+first_ind-1
        NSI(pntr) = (sum(NSI(pntr-4:pntr-1))+ current_noize)/5;
    else
        NSI(pntr) = current_noize;
    end
    
    Vol(pntr) = sum(Sp(round(150/bin_f)+1:round(2000/bin_f)+1,pntr));
    avg_bin_vol = Vol(pntr)/((round(2000/bin_f)+1)-(round(250/bin_f)+1)+1);
    
    % ��������������� ����������� ������� ������ �� SNR � HNR
    SNR_to_store = avg_bin_vol/NSI(pntr);
    
    last_stage = 0;
    avg_HNR = 0;
    if pntr>1
        last_stage = F0(pntr-1);
        avg_HNR = (HNR(pntr-1)+HNR(pntr))/2;
    end
    new_stage = last_stage;
    if last_stage==0 && ((avg_HNR > good_HNR) && (SNR_to_store > bad_SNR)) || ((SNR_to_store > good_SNR) && (avg_HNR > (not_too_bad_HNR+good_HNR)/4))
        new_stage = 1;
    elseif (((avg_HNR < not_too_bad_HNR) && (SNR_to_store < bad_SNR))) || (SNR_to_store < 1)
        if (SNR_to_store < 1)
            HNR(pntr) = HNR(pntr)/3;
        end
        new_stage = 0;
    end
    
    F0(pntr) = new_stage;
    
    % �������� ������ ���������� �� ����� ������� (����� ����� �����
    % �������)
    F0_Cand(pntr+first_ind-1,:,:) = Candidates(:,:);
    
    % �������� ���������� ������� (������ ��� Matlab)
    
    if sum(Candidates(:,1)~=0)>0
        Debug(pntr,1) = min(Candidates(Candidates(:,1)~=0,1));   % ����������� HNR
    end
    Debug(pntr,2) = avg_HNR;                % HNR
    Debug(pntr,3) = SNR_to_store;           % SNR
    Debug(pntr,5) = new_stage;              % ��������������� VAD
    
end