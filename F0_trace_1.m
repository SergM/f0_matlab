% ��������� F0 �� ���������� (F0_Cand) � ����������� ������ VAD
% ��� ����������� ����� ���������� ����� ��� ������ ������� ���������
function F0_trace_1(pntr, delay,HNR_slope,max_HNR,betta,FR_slope,FR_sigm_width,gamma,PWR_slope,zcross_PWR,delta,freqWidth)

    global voice;       % ������ �������� VAD
    global F0;          % ������ �������� F0
    global HNR;         % ������ �������� HNR
    
    % ������� ���� ��� ����� ������ ���� �� ������, ��� ����������
    % ���������
    deep = delay - 2;
    direct_deep = 100;

    % ���������� ��� ������������ F0 � ����
    defined_F0 = sum(F0(pntr:pntr+deep-1)>1);
    
    % ���������� VAD>0 � ����� ����� ����
    %   0011110011 - 0
    %   0111100111 - 0
    %   1111001111 - 4
    %   1110011110 - 3
    
    non_zero_VAD = 0;
    for i=pntr:pntr+deep-1
        if voice(i)>0
            non_zero_VAD = non_zero_VAD + 1;
        else
            break;
        end
    end
    
    % �������� ������� ���� �� �������� ��� ���������
    if (non_zero_VAD - defined_F0 <= 0) || (defined_F0 > 1)
        return;
    end

    % ����� ���� ���������� ����������� ��������� VAD ������ �� ������� 0
    win_end = pntr + non_zero_VAD - 1;
    
    % �������� ������� ���� � ������ �����
    if (voice(pntr-1) == 0) || (pntr == 1)
        % ��� ������ �����
        % ������������ ����� ���� ������������ HNR
        max_HNR = 0;
        j = 0;
        max_HNR_ind = 0;
        for i = pntr:win_end
            
            if max_HNR < HNR(i) && j>0
                max_HNR = HNR(i);
                max_HNR_ind = i;
            end
            j=j+1;
        end
        direction = -1;
        
        end_check_pntr = pntr;
        start_check_pntr = max_HNR_ind+1;
    else
        direction = 1;
        
        start_check_pntr = pntr;
        % ������������ ����� ����� ��� ������ ���������
        
        if non_zero_VAD < direct_deep
            win_end = pntr + non_zero_VAD - 1;
        else
            win_end = pntr + direct_deep - 1;
        end
        end_check_pntr = win_end;
    end
    
    % ������ ��� ����� ���� �������� �����������
    %if start_check_pntr==end_check_pntr
    %    fprintf('Start = %d  stop = %d   dir = %d\n',start_check_pntr, end_check_pntr, direction);
    %end
    tracer_1(start_check_pntr, end_check_pntr, direction,HNR_slope,max_HNR,betta,FR_slope,FR_sigm_width,gamma,PWR_slope,zcross_PWR,delta,freqWidth); 
    
end