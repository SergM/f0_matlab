function F0_cand7(pntr, Fd, fmin, fmax, N_candidates)

    global Sp F0 HNR;
    
    %global SNR NSI F0_Cand;
    
    %   Sp - ����������� ������������������� ������ �� ����
    %   Fd - ������� �������������
    %   fmin - ����������� ����������� ������� ��������� ���� (���)
    %   fmax - ������������ ����������� ���
    %   N_candidates - ���������� ����������� ���������� ���

    % ���������� ���������� (�� ����� ������� � ��������)
    good_HNR = 6;
    bad_HNR = 2.5;
   
    N = size(Sp,1);   % ���������� ����� � �������������� �������
    bin_f = Fd/2/N;   % ������� �� ���� ���
    
    % ������ � ��������� ���������� � ���
    Candidates = zeros(N_candidates,7);% 1-� - ���������-��� � ������
                                       % 2-� - ������� ��������� ���� (���)
                                       % 3-� - ������� �������� � ������
                                       % 4-� - ��� � ������
                                       % 5-� - ������
                                       % 6-� - ������� ������ ��������� ���
                                       % 7-� - ���������� �� ��������� ������������ ���
    
    
%     % ��� �������     
%     ccc = []; % ���������� ������
%     fff = []; % ���������� ������
    
    f = fmin;
    check_harm = 5;
    VocalHNR = 10^100;
    
    % ������� ����� ������������� ���� ������� � ��������� �� 4000��
    [max_pwr,max_pwr_bin] = max(Sp(1:round(1500/bin_f),pntr));
    
    
    if max_pwr > 0
        Dithering_factor = max_pwr/1000;
    else
        Dithering_factor = 1E-6;
    end
    
    Dithering_factor = 1E-7;
    
    while f<fmax
         
        % ��������� ��� ���� ��� ����������� ���������-��� �� ������
        % check_harm ����������
        harmonics = 0;

        % ���������� ������� ��� ���� � ������� � ������� ����
        
        % ������� ������ ��� ��������� �������
        first_bin = round(f/bin_f)+1;
        % ������� ��������� � ������������� ���� ������� ��� � ���������
        rate = round(max_pwr_bin/first_bin);
        if rate > check_harm
            start_c = rate - check_harm + 1;
        else
            start_c = 1;
        end
        % ���������� check_harm �������� ������� �� start_c
        
        for c = start_c:(start_c + check_harm - 1)
            current_bin = round(f*c/bin_f)+1;
            if current_bin > 6
                harmonics = harmonics + Sp(current_bin,pntr)+...
                    Sp(current_bin+1,pntr)+Sp(current_bin-1,pntr);
            end
        end
        % ��� - ��� �� ��������� (��������� �� ���������� �����)
        noize = (sum(Sp(5:current_bin+1,pntr))-harmonics)/...
            ((current_bin)-3*check_harm-5);
        
        % ������ ���� ��������� �� ���������� �����
        harmonics = harmonics/(3*check_harm);
        prev_VocalHNR = VocalHNR;
        VocalHNR = ((harmonics+Dithering_factor)/(noize+Dithering_factor));
        
        
%         % ��� �������
%         fff = [fff f];
%         ccc = [ccc VocalHNR];

        
        insert_pos = 0;
        lowest_level = 10^100;
        if (VocalHNR > 1) && (VocalHNR > prev_VocalHNR) 
            for i = 1:N_candidates
                % ���������� ���� ����������
                % ���� ������� ������, �� �������� ������� � SNR
                if (f - Candidates(i,2)) <= bin_f
                    % ���� ������� ����, �� ��������
                    if (VocalHNR >= Candidates(i,1))
                        Candidates(i,1) = VocalHNR;
                        Candidates(i,2) = f;
                        %Candidates(i,3) = harmonics;
                        %Candidates(i,4) = noize;
                        Candidates(i,6) = Sp(round(f/bin_f)+1,pntr);
                        Candidates(i,5) = noize*2 >= Sp(round(f/bin_f)+1,pntr);
                        insert_pos = 0;
                        break
                    end
                else
                    if (VocalHNR >= Candidates(i,1))
                        % ���������� ����������� ������� ��������� � ���
                        % �������
                        if Candidates(i,1) < lowest_level
                            lowest_level = Candidates(i,1);
                            insert_pos = i;
                            if Candidates(i,1) == 0
                                % ���� ������ ���, �� ����� ������ �� ����������
                                break
                            end
                        end
                    end
                end
            end    
            % ���� ����� ����� ��� � �� ��������
            if insert_pos ~= 0
                % �� �� � �� ���� ���������, ���� ����� ���� ����� �������
                % �����
                if insert_pos > 1
                    prev_cand_freq = Candidates(insert_pos-1,2);
                else
                    prev_cand_freq = 0;
                end
                if ((f - prev_cand_freq) > bin_f)
                    Candidates(insert_pos,1) = VocalHNR;
                    Candidates(insert_pos,2) = f;
                    %Candidates(insert_pos,3) = harmonics;
                    %Candidates(insert_pos,4) = noize;
                    Candidates(insert_pos,5) = noize*2 >= Sp(round(f/bin_f)+1,pntr);
                    Candidates(insert_pos,6) = Sp(round(f/bin_f)+1,pntr);
                end
            end
        end
        f=f+bin_f/check_harm;
    end
    
%     % ��� �������
%     if pntr == 147
%         figure('Name','Debug');
%         subplot(2,1,1);
%         plot(fff,ccc,'LineStyle','-','LineWidth',2);
%         hold on;
%         plot(Candidates(:,2),Candidates(:,1),'LineStyle','none','LineWidth',2,'Marker','o');
%         plot(Candidates(:,2),Candidates(:,6).*100,'LineStyle','none','LineWidth',2,'Marker','x');
%         xlim([0 2*fmax]);
%         subplot(2,1,2);
%         bar(0:bin_f:Fd/2-bin_f,Sp(:,pntr));
%         xlim([0 2*fmax]);
%     end
    

    % ���� ��������� ��������� ������� ��������� ����
    k = pntr-2;
    last_nz_fr = 0;
    last_nz_fr_lvl = max_pwr;
    % ���� �� ������ 256 ����� (256*256/16000 = 4,096�)
    if pntr > 256
        while k > pntr - 256
            k = k - 1;
            if F0(k)~=0
                last_nz_fr = F0(k);
                % �������� ������� ������� �� ������� ��������� ���������
                % ��� 3-� ����� ����� ������� � ��������� ���������
                last_nz_fr_lvl = mean(Sp(round(f/bin_f)+1,k-3:k));
                break;
            end
        end
    end
    
    
    % ��������� ���������� �� ��������� ��������� ���
    for k = 1:N_candidates
        Candidates(k,7) = abs(last_nz_fr - Candidates(k,2))/last_nz_fr;
        if Candidates(k,7) < 0.1
            Candidates(k,5) = 0;
        end
    end
    
    % �� ��������� ��� ���, ��� ������������ HNR
    [max_HNR, ind] = max(Candidates(:,1));
    F0_selected = Candidates(ind,2);
    
    % ���� HNR ���� ������������ ������, �� ��� = 0
    if (max_HNR < bad_HNR)
        F0_selected = 0;
    elseif ((max_HNR >= bad_HNR) && (max_HNR <= good_HNR)) 
        
        while 1
            prev_ind = ind;
            if Candidates(ind,5) == 1
                [~, ind] = min(Candidates(:,7));
                F0_selected = Candidates(ind,2);
                if prev_ind == ind
                    not_sure = 1;
                    break;
                end
            else
                % �������� ������������� � ����������� ��� �� ������������� �������
                % ������� ����� ������������ ���������� � ����� ������ ������
                % ���������� ������ (bad_HNR)
                % ��� ���� �� ������������ ����������� ��������� ������ �������������
                % �� ������� (� �������� 2-� �����)
                not_sure = 0;
                for i = 1:N_candidates
                    if i~=ind
                        if (max_HNR - Candidates(i,1)) < max_HNR * 0.25
                            not_sure = 1;
                            break;
                        end
                    end
                end
                break;
            end
        end
        if not_sure == 1

            [min_fr_diff, ind] = min(Candidates(:,7));

            if (min_fr_diff < 1.3) && (Candidates(ind,5)~=1) && (Candidates(ind,1) > bad_HNR)
                F0_selected = Candidates(ind,2);
            else
                F0_selected = 0;
            end
             
        else
            

            if last_nz_fr ~= 0
                if Candidates(ind,7) >= 0.7
                    [min_fr_diff, ind] = min(Candidates(:,7));

                    if min_fr_diff < 1.3 && (Candidates(ind,1) > bad_HNR)
                        F0_selected = Candidates(ind,2);
                    else
                        F0_selected = 0;
                    end
                end
            else
                F0_selected = Candidates(ind,2);                
            end


        end

        if Candidates(ind,6) < last_nz_fr_lvl*0.4
            F0_selected = 0;
        end
        
    else
        not_sure = 0;
        for i = 1:N_candidates
            if i~=ind
                if (max_HNR - Candidates(i,1)) < max_HNR * 0.25
                    not_sure = 1;
                    break;
                end
            end
        end
        if (not_sure == 1) || ((Candidates(ind,5)==1))

            [min_fr_diff, ind] = min(Candidates(:,7));

            if (min_fr_diff < 1.3) && (Candidates(ind,5)~=1) && (Candidates(ind,1) > bad_HNR)
                F0_selected = Candidates(ind,2);
            else
                F0_selected = 0;
            end
             
       end
        
    end
    
    if pntr > 2
        if abs(F0_selected - F0(pntr-2)) <= 0.1*F0_selected
            F0_2_mid = (F0_selected + F0(pntr-2))/2;
            if abs(F0(pntr-1)-F0_2_mid) > (F0_2_mid/1.05)
                F0(pntr-1) = F0_2_mid;
            end
        end
    end
    
    if F0_selected == 0
        [HNR(pntr), ind] = max(Candidates(:,1));
        if Candidates(ind,5) == 1
            HNR(pntr) = 0;
        end
    else
        HNR(pntr) = Candidates(ind,1);
    end

%    F0_Cand(pntr,:) = Candidates(:,2);
%    SNR(pntr,:) = Candidates(:,1);
    F0(pntr) = F0_selected; 
%    NSI(pntr,:) = Candidates(:,5);
    
end