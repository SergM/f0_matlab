function [period_array, next_start] = period(pntr, step, Fd, next_start, corr_treshold)

    global F0 Sound;

    % pntr - ������� � ����� �������� 
    % step - ��� �������� ����
    % win_size - ������ ����
    % F0 - ������ � ��������� ���������� ���� ��� ������ ������� ����
    % Sound - ������ �� ������
    period_array = [];

    if F0(pntr)~=0 || (F0(pntr-1)~=0 && F0(pntr)==0)
    
        % ��������� ��������� ������ ���� � ������� ����� ()
        cursor = ((pntr - 1) * step) + 1; % +1 � �++ ���� ������

        % ��������� ����������� � ������������ �������� ������� ���
        % ��������������

        tollerance = 0.3;
        if (F0(pntr-1)~=0 && F0(pntr)==0)
            min_shift = round(Fd / F0(pntr-1) * (1 - tollerance));
            max_shift = round(Fd / F0(pntr-1) * (1 + tollerance));
        elseif F0(pntr)~=0
            min_shift = round(Fd / F0(pntr) * (1 - tollerance));
            max_shift = round(Fd / F0(pntr) * (1 + tollerance));
        end

       
        % ��������� �������� � ����������� ����
        t = next_start;
        
        while t < step
            
            cut = Sound(cursor+t:cursor+t+max_shift);
            cut_size = max_shift;

            corr = zeros(max_shift - min_shift + 1,1);
            for i = min_shift:max_shift
                cur_corr = 0;
                for j = 1:cut_size
                    cur_corr = cur_corr + cut(j) * Sound(i+j+cursor+t-1);
                end
                
                corr(i-min_shift+1) = cur_corr;
            end

            % ���� �������� ����������������� �������
            max_corr = 0;
            ind = 0;
            for i=2:size(corr,1)-1
                % �� �������� ��������� ����� ����� � �������� ������
                if (corr(i-1)<corr(i)) && (corr(i+1)<corr(i))
                    if max_corr<corr(i)
                        max_corr = corr(i);
                        ind = i;
                    end
                end
            end
                        
            % ��������� �������� ������
            real_period = ind + min_shift;
            %���� ������� ������ �������
            [pulse, ind] = max(abs(Sound(cursor+t:cursor+t+real_period)));

            % cursor+t+real_period
            cur_pntr = round((cursor+t+real_period)/step);
            cur_frequency = F0(cur_pntr);
            new_frequency = Fd/real_period;
            fr_diff = abs(new_frequency-cur_frequency);
            
            %if ((cur_frequency==0) && (max_corr/pulse/pulse > 10)) || ((cur_frequency~=0) && (cur_frequency * 0.3 > fr_diff) && (max_corr > corr_treshold))
            if (max_corr/pulse/pulse > 20)
                period_array = [period_array; [cursor+t+ind real_period pulse max_corr]];
                % ��������� F0 �� ������� �������
                F0(cur_pntr) = Fd/real_period;
            else
                F0(cur_pntr) = 0;
            end

            t = t + real_period+ind;%-round(min_shift/4);
        end
        % ��� ����� ��������� ����� ���������� ����� �� ��������� ����
        next_start = t - step; 
    end
end