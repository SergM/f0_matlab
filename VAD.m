function VAD(pntr, before, after, max_back_gap, max_prolongation, glue_gap, extra_before, extra_after)

    global F0 voice HNR;
    
    HNR_treshold = 2.5;

%   F0 - ������ � ���
%   voice - ������ � ���������� ������
%   (0 - ��� ������, 1 - �������������� �������, 2 - �� �������� ��������������, 3 - ������������ ��������� ��� �����������)
%   pntr - ��������� �� ������� ����
%   before - ����� � ����� ���� �� ��������������� �������, ����������� ������� 
%   after - ����� � ����� ���� ����� ��������������� �������, ����������� ������� 
%   max_back_gap - ������������ ���������� ����� �����, �� ������� ��������
%                  ���������
%   max_prolongation - ������������ ���������� ����� ������, �� ������� ��������
%                  ���������
%   

    if pntr>2
        if F0(pntr)>0

            % ���� ������ �������� �������������� �������, �� ���� ��������� � ���������
            % ���������� ����� ���������, ��������������� ������������
            % ������� 
            % ����������� �� �����, � ������� HNR > 2 (����������� before ���������)

            % ��� �������������� ������� ����������, ��� 1
            voice(pntr) = 1;

            if voice(pntr-1)==0

                k = pntr-1;

                while 1
                    % ������, ���� ��� ���-�� ��������� ��������, �� ��
                    % ���� ��������
                    % � ���� ����������� ��� ����������� 3-����, �� ����
                    % ���������
                    % �� ����� �� ������ �����
                    if (k-before-1) <= 0
                        break;
                    end
                    % �� ���� ����� ������� ������ �����
                    if (pntr-k) >= max_back_gap
                        break;
                    end

                    if voice(k) >= 2
                        break;
                    end
                    
                    v_det = max(HNR(k-before-1:k)) > HNR_treshold;

                    if v_det == 1
                        if voice(k)==0
                            % ��� ��� � �������� �������
                            if HNR(k) > HNR_treshold   
                                voice(k) = 2;   
                            else                
                                voice(k) = 3;                                 
                            end                                            
                        end
                    else
                        if max(voice(k-glue_gap:k)) > 0
                            % ���� � �������� ������������ ��������� ����
                            % �������, �� ���� ��������� �� 4-����
                            for g = k-glue_gap-1:k
                                if voice(g) == 0
                                    voice(g) = 4;
                                end
                            end
                        end
                        for g = k-extra_before+1:k
                            if voice(g) == 0
                                voice(g) = 4;
                            end
                        end
                        break;
                    end
                    
                    k = k - 1;
                end
            end
        elseif (F0(pntr)==0) && (F0(pntr-1)>0)
            % ���� ����� �������������� �������, �������� ���������
            % ����������� �����, ��� ����������� �����������
            
            k = pntr;
            while 1
                % ������, ���� ��� ���-�� ��������� ��������, �� ��
                % ���� ��������
                % � ���� ����������� ��� ����������� 3-����, �� ����
                % ���������
                if voice(k) >= 2
                    break;
                end
                % �� ���� ����� ������� ������ ������
                if (k - pntr + after) >= max_prolongation
                    break;
                end

                v_det = max(HNR(k:k+after)) > HNR_treshold;

                if v_det == 1
                    if voice(k)==0
                        % ��� ��� � �������� �������
                        if HNR(k) > HNR_treshold
                            voice(k) = 2;
                        else
                            voice(k) = 3;
                        end
                    end
                else
                    for g = k:k+extra_after-1
                        if voice(g) == 0
                            voice(g) = 4;
                        end
                    end
                    break;
                end
                k = k + 1;
            end
        end
    end
end