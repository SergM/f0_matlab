
for f_num = [17]
    f_name = strcat('llk\',num2str(f_num),'.csv');
    llk = importdata(f_name,';',1);

    
    

    t1 = 0; % ������ ��� ������� �1
    t2 = 0; % ������ ��� ������� �2
    llk_diff_treth = 2; % ����� ������� llk
    max_alarm_t = 60;   % ���������� ������� ��� ���������� ������ ��� ������� ������������
    alarm1 = zeros(size(llk.data(:,1)));
    alarm2 = zeros(size(llk.data(:,1)));
    X_ = [1:length(llk.data(:,1))]'.*250./16000;
    dict_diff = zeros(size(llk.data(:,1)));
    for i = 1:length(llk.data(:,1))
        dict_diff(i) = llk.data(i,1)-llk.data(i,2);
        % ���� ������� ������ ����, �� ������� ������ �1
        if dict_diff(i)>0
            if dict_diff(i) < llk_diff_treth
                t1=t1+1;
                if t1>max_alarm_t
                    alarm1(i)=20;
                end
            else
                % ������ �1 ��������� ��������
                t1=0;
                %t2=0;
            end
        % ���� ������� ������ ����, �� ������� ������ �1
        else
            if dict_diff(i) > -llk_diff_treth
                t2=t2+1;
                if t2>max_alarm_t
                    alarm2(i)=20;
                end
            else
                % ������ �2 ��������� ��������
                %t1=0;
                t2=0;
            end
        end
    end


    figure('Name',f_name);
    %subplot(2,1,1);
    plot(X_,alarm1,'LineWidth',3)
    hold on
    plot(X_,alarm2,'LineWidth',3)
    %subplot(2,1,2);
    bar(X_,dict_diff);
    legend('Alarm_1','Alarm_2','diff llk(1-2)');
end