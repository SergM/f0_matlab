    % ���������� ��������� �� �������

    % ��� ���������� ��������� �� �������
    bands = [200,500,2000,5000,7000];
    band_envelope = zeros(stop_step-start_step+1,size(bands,2));
    % �������� ������ ����� ��� ���������
    can_get_bands_envelope = 1;
    for i=1:size(bands,1)-1
        if bands(i+1)-bands(i) < bin_f*2
            can_get_bands_envelope = 0;
            break;
        end
    end    
    
    if can_get_bands_envelope==1
        % ����� ������������ ��������������� ������ � ������ �������� ����
        moment_sp = Sp(:,i);
        start_bin = 1;
        cur_freq = 0;
        cur_band = 1;
        bins = 0;
        summ = 0;
        for j = 1:size(moment_sp,1)
            % �������� �������, ��������������� ����
            cur_freq = (j-1)*bin_f;
            % �������� ��������� �������� � ������
            % summ = summ + 10^(moment_sp(j)/20); % ��� ��� ��� ���-�������
            summ = summ + moment_sp(j);
            bins = bins+1;
            if cur_band < size(bands,2)
                if cur_freq > bands(cur_band)
                    band_envelope(i-start_step+1,cur_band) = summ/bins/bin_f;
                    summ = 0;
                    bins = 0;
                    cur_band = cur_band + 1;
                end
            end
        end
    end
    band_envelope(i-start_step+1,cur_band) = summ/bins/bin_f;