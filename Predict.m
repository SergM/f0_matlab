global Sound LP_ERR;

% ��������� �������� ����
% 16000 ��, 16���, ����
filename = 'WAV\F0_1.wav';
%filename = 'sample5_N100.wav';
%filename = 'sample5.wav';
%filename = 'Noise_80_2_16000.wav';
[Sound_,Fs] = audioread(filename);

% ����� ���������
Length = size(Sound_,1);
%Length = 16000*4;

%% �������� � �++
bw_treshold = 600;
%%


Fd = 16000;
WSize = 1024;    % ������������ ���� ����� 20 �� (����� ������� �� 4)
Step = WSize/4;     % ��� ��������� 10 ��
LPC_Order = 17; % ������� �������� LPC
min_per = round(Fd/500);
max_per = round(Fd/80);

quart_win = WSize/4;

% ��������� ������������� ������� ��� �����������
YSize = fix(Length/Step)-2; % ����� ������� � ������ ���� ���������

LP_ERR = zeros(Length,1);
LPC = zeros(Length,1);
new_LP_ERR = zeros(Length,1);
CORR = zeros(Length,1);

new_Win = zeros(WSize,1);
win1 = zeros(WSize+Step,1);
win2 = zeros(WSize+Step,1);

formants = zeros(YSize,5);
formant_pwr = zeros(YSize,5);

%Sound = filter(1, [1 0.99], Sound_);
Sound = Sound_;


% �������� ����
% ��� ��������� � ���������� ����
i = 1;
tic

Spectr=zeros(YSize,WSize);
Window = zeros(WSize,1);

while i <= YSize-2
    % �������� ����
    %Window = Sound(Step*i+1:Step*i+WSize,1);
    Window(1:WSize) = Sound(Step*i+1:Step*i+WSize,1);
    %Window_ = Sound(Step*i+1:Step*i+WSize,1);
    %preemph = [1 0.63];
    %Window = filter(1,preemph,Window_);
    
    % �������� ������������ �������� LPC
    Coeff = (lpc(Window,LPC_Order))';
    
    %win1(1+quart_win:WSize/2+quart_win) = new_Win(1+quart_win:WSize/2+quart_win);
    
    % ������� ������ ������������
    %new_Win = LPC_Filter_v2(Window,Coeff);
    new_Win = LPC_Filter_d(Window,Coeff);
    
    %win2(1+quart_win+Step:WSize/2+quart_win+Step) = new_Win(1+quart_win:WSize/2+quart_win);
    
    LP_ERR(Step*i+1+quart_win:Step*i+WSize/2+quart_win) = new_Win(1+quart_win:WSize/2+quart_win);
    LPC(Step*i+1+quart_win:Step*i+WSize/2+quart_win) = Window(1+quart_win:WSize/2+quart_win)-new_Win(1+quart_win:WSize/2+quart_win);
  
    %Spectr(i,:) = abs(fft(Window(1+quart_win:WSize/2+quart_win)-new_Win(1+quart_win:WSize/2+quart_win),WSize));
    Spectr(i,:) = abs(fft(new_Win,WSize))./LPC_Order;
    
    rts = roots(Coeff);
    rts = rts(imag(rts)>=0);
    angz = atan2(imag(rts),real(rts));
    [frqs,indices] = sort(angz.*(Fs/(2*pi)));
    bw = -1/2*(Fs/(2*pi))*log(abs(rts(indices)));

    nn = 1;
    for kk = 1:length(frqs)
        if (frqs(kk) > 0 && bw(kk) < 1000)
            formants(i,nn) = frqs(kk);
            formant_pwr(i,nn) = bw(kk);
            nn = nn+1;
            if (nn>5)
                break;
            end
        end
    end
    
% �������� ����������� ����� ����� ������� ���� ��� ����.
% ��������� ������� �������� � �����������.
% �������� ������� � ������������ ��������
    if i>2
        % ��� ���� ��������� ������� �������� �������
        % ������� ���������� ���� � ������� �� �����������
        old_distance = (formants(i,2) - formants(i-1,2))^2 + ...
                       (formants(i,3) - formants(i-1,3))^2 + ...
                       (formants(i,4) - formants(i-1,4))^2;
        formant_set = 2;
        %% �������� ������� � �++
        if (formant_pwr(i,1) > bw_treshold) || (formant_pwr(i,2) > bw_treshold) || (formant_pwr(i,3) > bw_treshold)
            for j = -1:2:1 % � ����� 2
                distance = (formants(i,2+j) - formants(i-1,2))^2 + ...
                           (formants(i,3+j) - formants(i-1,3))^2 + ...
                           (formants(i,4+j) - formants(i-1,4))^2;
                if old_distance > distance
                    old_distance = distance;
                    formant_set = j+2;
                end
            end
        end
        %%
        % ��������� ����� formant_set
        % ������, ���� ����������� � ������ �� ������� (������ ����)
        % �.�. ������, ����� ����������

        if formant_set == 1
            formants(i,5)=formants(i,4);
            formants(i,4)=formants(i,3);
            formants(i,3)=formants(i,2);
            formants(i,2)=formants(i,1);
            formants(i,1)=0;
        elseif formant_set == 3
            formants(i,1)=formants(i,2);
            formants(i,2)=formants(i,3);
            formants(i,3)=formants(i,4);
            formants(i,4)=formants(i,5);
            formants(i,5)=0;
        end
    end
    
    
    i = i + 1;
    
end
toc

% �������

X  = 0:1/Fd:(Length-1)/Fd; % ����� ��� ����������� ������� �������
End_of_time = (Length-1)/Fd;

x_lim = [1 378];
st = 201;
SFFT = abs(fft(Sound(1+256*st:(WSize)+256*st),1024));

FF = (0:(x_lim(2)-1)).*(Fd/1024); 
max_SFFT = max(SFFT(1:x_lim(2)));
SFFT(1:x_lim(2)) = SFFT(1:x_lim(2))./max_SFFT;
max_Spectr = max(Spectr(st,1:x_lim(2)));
envel = (Spectr(st,1:x_lim(2))./max_Spectr)';

figure;
bar(FF ,SFFT(1:x_lim(2)),0.8)

hold on
plot(FF, envel);

bar(FF ,SFFT(1:x_lim(2))./envel,0.5)


xlim(x_lim*(Fd/1024));

hold off


% figure('Name',filename);
% 
% x_lim = [0 7];
% 
% % ���������� ����
% subplot(4,1,1);
% plot(X,LPC(1:Length));
% hold on
% plot(X,Sound(1:Length));
% legend({strcat('�������� ������������',sprintf(' %d',LPC_Order)),'������'},'FontName','Arial','FontSize',8);
% title('������������','FontName','Arial','FontSize',10)
% 
% xlim(x_lim);
% 
% %hold on;
% 
% subplot(4,1,2);
% plot(X,LP_ERR);
% hold on;
% plot(X,new_LP_ERR.*LP_ERR);
% title('������ ��������� ������������','FontName','Arial','FontSize',10)
% xlim(x_lim);
% 
% X_ = 0:1/Fd*Step:(Length-Step)/Fd; % ����� ��� ����������� ������� � �������� �����
% 
% subplot(4,1,3);
% plot(X_,formant_pwr(:,:));
% title('������ ��������','FontName','Arial','FontSize',10)
% xlim(x_lim);
% 
% subplot(4,1,4);
% plot(X_,formants(:,1:3),'LineStyle','none','LineWidth',1,'Marker','.');
% title('��������','FontName','Arial','FontSize',10)
% xlim(x_lim);
% 
% %ylim([-20 40]);



