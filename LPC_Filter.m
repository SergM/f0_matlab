function win = LPC_Filter(Window, Coeff)
    win_Size = size(Window,1);
    win = zeros(win_Size,1);
    Order = size(Coeff,1);
    new_win = zeros(win_Size+Order,1);
    new_win(Order+1:Order+win_Size) = Window;
    for i = 1:win_Size
        accum = 0;
        for j=1:Order
            accum = accum + Coeff(j) * new_win(i-j+Order);
        end
        win(i) = accum;
    end
end