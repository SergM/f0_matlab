function [R, Lag] = My_Corr(Frame, minLag, maxLag)

% ����� ��������� �������������� � ��������� �����
% Frame - ������ ������
% minLag - ����������� ���
% maxLag - ������������ ���
% ����������
% R - ������������ �������� ������������������ �������
% Lag - ��������������� ��� ���

    N = size(Frame,1);  %����� ������

    fR = zeros(maxLag-minLag+1);
    prev = 0;
    preprev = 0;
    
    R = 0;
    Lag = 0;
    
    for i=minLag:maxLag
        prod = Frame(i+1:N).*Frame(1:N-i);
        r = sum(prod);
        if (prev >= r) && (preprev <= prev) && (r > 0) && (prev > 0) && (preprev > 0) && (R-(R*0.05) < prev)
            R = prev;
            Lag = i-1;
        end
            
        fR(i-minLag+1) = r;
        
        preprev = prev;
        prev = r;
 
    end

end