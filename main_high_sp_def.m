%clear all;

global Sound Sp F0 Debug NSI thesh_gain Volume F0_Cand voice PRE_VOICE hi_fr_voice mean_noize HNR;
global Vol;

% ����� ������� ������ ���� ����� ���� �������� ������ ����������� ������
% � ��� ����� ���������:
% - ��� ����
% - ������������� (�������������������)
% - ������� F0, F1, F2, F3 ��� ������� ���� ���������
% - ����������� ������-���
% - ������� ���� ��� �������(Noise Strength Index)
% - ���������
% ��������, ��� ���-��

% ��������� �������� ����
% 16000 ��, 16���, ����

Fd = 16000;
% ������ ���� ���� �� ������ ������� 2^n
WSize = 1024;   % ������������ ���� ����� 64 ��

%WSize = 4096;   % ������������ ���� ����� 64 ��
Step = 256;     % ��� ��������� 16��

F0_N_Cand = 8;

thesh_gain = 1;
Last_F0 = 0;

%filename = 'E:\F0_Dima\�������\V_000_0042_178.wav';     



%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0002_129_1.wav';
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0004_131_13.wav';
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0002_129_9.wav';
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0055_199_5.wav';     
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0060_207_2.wav';         % 200 600 200
%filename = 'E:\F0_Dima\f0_matlab\short\Record_3.wav';

filename = 'F:\2020-02-17_Audio_Transfer\V_000_0066_214.wav';
%filename = 'WAV\Interview_2.wav';
%filename = 'WAV\Interview_3.wav';    % 200 600 200
%filename = 'WAV\Interview_5.wav';
%filename = 'WAV\������ 6.wav';
%filename = 'WAV\V_000_0073_217.wav';                                                                
%filename = 'WAV\MicRecord_Tuesday_2_June_2020_19.11.24.wav';
%filename = 'WAV\MicRecord_Tuesday_2_June_2020_19.13.40.wav';

%filename = 'noized1.wav';
%filename = 'noized2.wav';
%filename = 'E:\111.wav';
%filename = 'WAV\noized1.wav';
%filename = 'WAV\Noise_100.wav';
%filename = 'WAV\sample.wav';
%filename = 'WAV\sample0.wav';
%filename = 'sample1.wav';
%filename = 'sample2.wav';
%filename = 'sample4.wav';
%filename = 'sample5.wav';
%filename = 'sample5_filtred.wav';
%filename = 'sample5_filtred_300-3400.wav';
%filename = 'sample5_N30.wav';
%filename = 'sample5_N100.wav';
%filename = '������ (15).wav';
%filename = '..\..\Audi analitics\Interview_1.wav';
[Sound_,Fs] = audioread(filename);

%Sound_ = filter(1, [1 -0.5], Sound_);
if (size(Sound_,2)==2)
    Sound = resample((Sound_(:,1)+Sound_(:,2))/2,Fd,Fs);
else
    Sound = resample(Sound_(:,1),Fd,Fs);
end

% ����� ���������
Length = size(Sound,1);
%Length = 16000*100;

% ��������� ������������� ������� ��� �����������
YSize = fix(Length/Step); % ����� ������� � ������ ���� ���������
Sp = zeros(WSize/2, YSize);
Volume = zeros(YSize,1); % ��� ������ ���������

New_Sound = [];

mean_noize =  zeros(YSize,1);

voice = zeros(YSize,1); % ��� VAD
hi_fr_voice = zeros(YSize,1); % ��� ������� VAD

F0 = zeros(YSize,1);
PRE_VOICE = zeros(YSize,1);
F0_Cand = zeros(YSize,F0_N_Cand,6);
ZeroCross = zeros(YSize,1);
NSI = zeros(YSize,1);
Debug = zeros(YSize,6);
HNR = zeros(YSize,1);
Vol = zeros(YSize,1);
Sonogramm = zeros(WSize/2, YSize);  % ��� ����������� ����������
correl = zeros(2, YSize);  % ��� ��������������
spectrogramm = zeros(WSize/2, YSize);
cleansp = zeros(WSize/2, YSize);
LPC = zeros(9, YSize);
formants = zeros(9, YSize);
cccc = zeros(95, YSize);

%N_harm = 5; % ���������� ����������� ��������

fmin = 75;  % ����������� ����������� �������
fmax = 400;  % ������������ ����������� �������

bin_f = Fd/WSize;         % ������� �� ���� ���

Window = Sound(WSize:-1:1,1);
%Sound = [Sound; zeros(126*Step,1)];
%WSh = gausswin(WSize,2.5); % ���� ��� �������������� �����
ham = 0.53836 - 0.46164 * cos(2 * 3.1415 * [0:(WSize-1)] / (WSize - 1));
% �������� ����
% ��� ��������� � ���������� ����
% �� ������... ��������, ���-�� � ��������������� ����� ��� ���� �
% �����������������...
i = 1; % ������� ������


period_array = [];


% ��������� VAD
steps_before = 8; % 0.128 ���.
steps_after = 8;  % 0.128 ���.
glue_gap = 8;% 0.192 ���.
extra_before = 2; % 0.032 ���.
extra_after = 2;  % 0.032 ���.

max_back_gap = 63; % 1 ���.
max_prolongation = 126; % 2 ���. (max_back_gap + �������� ��������� ��� F0_Cand)


new_Vad_mask = uint8(zeros(1,1));
new_pointer = 1; %-1
jj=0;
tic

%flog = fopen('log.txt','a');


while i <= YSize-WSize/Step
    % �������� ����
    Window = Sound(Step*(i-1)+1:Step*(i-1)+WSize);
        
    % ��������� ������� ��������� ������
    Volume(i) = Avr_Vol(Window);
    
    % ������� ������ (�� ����) ������
    [Sp(:,i), Ph_sp] = Spectr(Window.*ham');

    Volume(i) = 20*log10(sum(Sp(round(150/bin_f)+1:round(700/bin_f)+1,i)));
    
    % ��������� ������� ��������� ����
    F0_cand9(i, Fd, F0_N_Cand);%, flog); % � ������ ������ ��� �������� ���������
    
%    F0_cand8(i, Fd, fmin, fmax, F0_N_Cand); % �������� ��������� -64 ����
    
    if (i>max_prolongation) && (i<YSize-WSize/Step-steps_after)
        % ��������� ����� ������ ���� ��� ������������ F0, � ��� �����
        % ���������� ������ ������
        VAD(i-max_prolongation, steps_before, steps_after, max_back_gap, max_prolongation, glue_gap, extra_before, extra_after);
    end

    if (i>max_prolongation+max_back_gap) && (i<YSize-WSize/Step-steps_after)
        % VAD ������ ���������� � ����������� � 
        % ���������� ������ ������
        F0_trace(i-max_prolongation-max_back_gap, max_back_gap);
    end
    
    % �������� �������������
    Sonogramm(:, i) = 20*log10(Sp(1:WSize/2,i));
    %Sonogramm_Ph(:, i) = Ph_sp;
    
    i = i + 1;
    
    if mod(i,1000)==0
        fprintf('���������� %d ����� �� %d\n',i,YSize-WSize/Step);
    end    
end

%fclose(flog);

toc


% �������� ������ � ������� �� ��������

Jitter = 0;
J_num = 0;
J_denom = 0;
S_num = 0;
S_denom = 0;
% ������ � ����� ��������� � ������� �����
interval_start = round(Fd*3.1);
interval_end = round(Fd*3.1);

% ������ � ����� ��������� � ����� ����������� ����
start_step = round(interval_start/Step);
stop_step = round(interval_end/Step)+2;

% �������� �� ����� �� ������� �������
if start_step < 4
    start_step = 4;
end

if stop_step > YSize-1
    stop_step = YSize-1;
end
N = 0;
prev_T = 0;
prev_A = 0;

next_start = 0;

% for i = start_step:stop_step
%     
%     if (F0(i-1)==0) && (F0(i-2)==0) && (F0(i-3)==0)
%         next_start = -2*Step;
%     end
%     
%     [new_period_array, next_start] = period(i, Step, Fd, next_start, 0.01);
%     for j = 1:size(new_period_array,1)
%         if (new_period_array(j,1) >= interval_start) && (new_period_array(j,1) <= interval_end)
%         
%             % ��� ������� �������� ������ (����� � �������� � ������, ���� ����)
%             period_array = [period_array; new_period_array(j,:)];
% 
%             % ���������� ���������� � ������������ ��� �������� � �������
% 
%             if N>1
% 
%                 J_num = J_num + abs(new_period_array(j,2)-prev_T);
%                 S_num = S_num + abs(new_period_array(j,3)-prev_A);
%                 prev_T = new_period_array(j,2);
%                 prev_A = new_period_array(j,3);
%             else
%                 prev_T = new_period_array(j,2);
%                 prev_A = new_period_array(j,3);
%             end
%             
%             J_denom = J_denom + new_period_array(j,2);
%             S_denom = S_denom + new_period_array(j,3);
% 
%             N = N + 1;
%         
%         end
%     end
% end
% 
% X = 0:1/Fd:(Length-1)/Fd; % ����� ��� ����������� ������� �������
% fprintf('���������� ���������: %d\n',N);
% Shimmer = (N*S_num)/((N-1)*S_denom);
% Jitter = (N*J_num)/((N-1)*J_denom);

%figure;
%plot(X,Sound(1:Length));
%hold on;

%ii = 192;
%plot([Step*(ii-1)+1:Step*(ii-1)+WSize]./16000,Sound(Step*(ii-1)+1:Step*(ii-1)+WSize));

%bar((period_array(:,1)-1)./Fd,period_array(:,3),'BarWidth',0.1,'FaceColor',[0 0 0]);
%plot((period_array(:,1)-1)./Fd, period_array(:,3),'LineStyle','none','LineWidth',3,'Marker','.');
%xlim([3.0 3.5]);


% fprintf('������: %2.8f\n', Shimmer);
% fprintf('�������: %2.8f\n', Jitter);

% fileID = fopen('SAD_mask.bin','w');
% fwrite(fileID,new_Vad_mask);
% fclose(fileID);

new_Vad_mask_1 = double(new_Vad_mask');

%save('SAD_mask.txt','new_Vad_mask_1','-ascii');
%audiowrite('test.wav',New_Sound, Fd);

% �������
MaxFr = 4000;   % ������������ ������� ����������� ����������
MaxSonY = fix((WSize/2)/(Fd/2)*(MaxFr-1))+1; %������������ ����� ���� ����������

X_ = 0:1/Fd*Step:(Length-Step)/Fd; % ����� ��� ����������� ������� � �������� �����

%X_lim = [0 (Length-1)/Fd];
X_lim = [0 8];
Y_lim = [0 1500];

figure('Name',filename);

% ��� ��� ����������
%subplot(2,1,2);
imagesc(X_,[0 MaxFr],Sonogramm(1:MaxSonY,:));
colormap(([255:-1:0;255:-1:0;255:-1:0])'./255);

title('�������������','FontName','Arial','FontSize',10)
ax = gca;
ax.YDir = 'normal';

% ��� ��� ���������
%subplot(3,1,3);
hold on;

plot(X_,F0_Cand(:,:,2),'LineStyle','none','LineWidth',1,'Marker','*');
plot(X_,F0(:,1),'LineStyle','none','LineWidth',2,'Marker','o');

bar(X_,voice.*20);

plot(X_,PRE_VOICE.*100+800,'LineWidth',2);

% max HNR
plot(X_,Debug(:,2).*100+1000,'LineWidth',2,'Color',[0.0,0.9,0.0]);
% min HNR
plot(X_,Debug(:,1).*100+1000,'LineWidth',2,'Color',[0.0,0.8,0.0]);

% Zero
plot(X_,zeros(YSize,1)+1000,'LineWidth',1,'Color',[0.5,0.5,0.5]);

tresh = 1.0;
plot(X_,ones(YSize,1).*tresh.*100+1000,'LineWidth',1,'LineStyle','--','Color',[0.0,0.7,0.0]);
tresh = 1.5;
plot(X_,ones(YSize,1).*tresh.*100+1000,'LineWidth',1,'LineStyle','--','Color',[0.0,0.7,0.0]);
tresh = 2.5;
plot(X_,ones(YSize,1).*tresh.*100+1000,'LineWidth',1,'LineStyle','--','Color',[0.0,0.5,0.0]);
tresh = 3;
plot(X_,ones(YSize,1).*tresh.*100+1000,'LineWidth',1,'LineStyle','--','Color',[0.0,0.7,0.0]);

% SNR
plot(X_,Debug(:,3).*20+1500,'LineWidth',2,'Color',[0.8,0.0,0.0]);

tresh = 0;
plot(X_,ones(YSize,1).*tresh.*20+1500,'LineWidth',1,'LineStyle','-','Color',[0.5,0.5,0.5]);

tresh = 1;
plot(X_,ones(YSize,1).*tresh.*20+1500,'LineWidth',1,'LineStyle','-','Color',[0.5,0.5,0.5]);

tresh = 10;
plot(X_,ones(YSize,1).*tresh.*20+1500,'LineWidth',1,'LineStyle','--','Color',[0.7,0.0,0.0]);

tresh = 3;
plot(X_,ones(YSize,1).*tresh.*20+1500,'LineWidth',1,'LineStyle','--','Color',[0.7,0.0,0.0]);

% ���
plot(X_,NSI.*1000.*1000+1000,'LineWidth',2,'Color',[0.0,0.0,0.8]);




xlim(X_lim);
ylim(Y_lim);
 
 %plot(X_,F0(:,2));
 %xlim([0 (Length-1)/Fd]);


 % ��� ��� ����������
%  subplot(3,1,3);
%  imagesc(X_,[0 MaxFr],Cocogramm(1:MaxSonY,:));
%  colormap(([255:-1:0;255:-1:0;255:-1:0])'./255);
%  ax = gca;
%  ax.YDir = 'normal';

 % ��� ��� ����������
%  subplot(3,1,3);
%  imagesc(X_,[0 MaxFr],SHC(:,:));
%  colormap(([255:-1:0;255:-1:0;255:-1:0])'./255);
%  ax = gca;
%  ax.YDir = 'normal';

% figure
% 
% MaxFr = 8000;   % ������������ ������� ����������� ����������
% MaxSonY = fix((WSize/2)/(Fd/2)*(MaxFr-1))+1; %������������ ����� ���� ����������
% 
% imagesc(X_,[0 MaxFr],Sonogramm(1:MaxSonY,:));
% colormap(([255:-1:0;255:-1:0;255:-1:0])'./255);
% title('�������������','FontName','Arial','FontSize',10)
% ax = gca;
% ax.YDir = 'normal';
% 
% hold on;
% 
% plot(X_,F0(:,1),'LineStyle','none','LineWidth',2,'Marker','o');
% xlim(X_lim);


changePoints = diff(voice>0);

segBeg = find(changePoints==1)+1;
segEnd = find(changePoints==-1);

% if audio start with speech
if voice(1)>1
    segBeg = [1;segBeg];    
end
% if audio ends with speech
if voice(end)>1
    segEnd = [segEnd;length(voice>0)];
end

delta_t = Step/Fd;

% ��������� ������ ��������
point_pos = max(strfind(filename,'.'));
%mask_path = '..\program\������ ��� ��������\';
mask_path = '';
mask_file = strcat(mask_path,filename(1:point_pos),'csv');

mask = importdata(mask_file,';',1);

%manual_mask = [mask.data(:,2:3),ones(size(mask.data(:,1))).*2];


fprintf('���������� � ������ ��������: %d\n',size(mask.data(:,1),1));
fprintf('���������� � �������������� ��������: %d\n\n',size(segEnd,1));

full_borders = [(segBeg(:)-0).*delta_t, ones(size(segEnd));
                (segEnd(:)-0).*delta_t, ones(size(segEnd)).*2;
                mask.data(:,2),  ones(size(mask.data(:,1))).*3;
                mask.data(:,3),  ones(size(mask.data(:,1))).*4];


full_borders = sortrows(full_borders,1);

% ���� ������������ ������ � �������
bord_diff = find((round(full_borders(2:end,1).*100000)./100000-round(full_borders(1:end-1,1).*100000)./100000)==0);
full_borders(bord_diff,1) = full_borders(bord_diff,1)+0.00001;
full_borders = sortrows(full_borders,1);


tp = 0;
tn = full_borders(1,1);
fp = 0;
fn = 0;

positive = 0;
negative = full_borders(1,1);


end_time = Length/Fd;

man_started = 0;
auto_started = 0;


manual_mask = zeros(size(voice));


for i = 1:(length(full_borders)-1)

    if full_borders(i,2) == 1
        auto_started=1;
    elseif full_borders(i,2) == 2
        auto_started=0;
    elseif full_borders(i,2) == 3
        man_started=1;
    elseif full_borders(i,2) == 4
        man_started=0;
    end
    
    dt = full_borders(i+1,1)-full_borders(i,1);
    
    if man_started == 1
        cur_pntr = round(full_borders(i,1)/delta_t);
        next_pntr = round(full_borders(i+1,1)/delta_t);
        for j = cur_pntr:next_pntr
            manual_mask(j) = 1;
        end
    end
    
    if     full_borders(i+1,2) == 1
        % ��������� ������� - ������ ��������������� ���������
        % �.�. ������ ��� ������ � �������������� ��������
        if man_started
            % �� ����� ���������� ���������� ������
            fn = fn + dt;
            negative = negative + dt;
        else
            % ����� ���������� ���������� ������
            tn = tn + dt;
            negative = negative + dt;
        end
    elseif full_borders(i+1,2) == 2
        % ��������� ������� - ����� ��������������� ���������
        % �.�. ������ ���� ����� � �������������� ��������
        if man_started
            % ����� ���������� ������� ������
            tp = tp + dt;
            positive = positive + dt;
        else
            % ������� ���������� ������� ������
            fp = fp + dt;
            positive = positive + dt;
        end
    elseif full_borders(i+1,2) == 3
        % ��������� ������� - ������ ������� ���������
        % �.�. ������ ��� ������ � ������ ��������
        if auto_started
            % ������� ���������� ������� ������
            fp = fp + full_borders(i+1,1)-full_borders(i,1);            
            positive = positive + dt;
        else
            % ����� ���������� ���������� ������
            tn = tn + dt;            
            negative = negative + dt;
        end
    elseif full_borders(i+1,2) == 4
        % ��������� ������� - ����� ������� ���������
        % �.�. ������ ���� ����� � ������ ��������
        if auto_started
            % ������� ���������� ������� ������
            tp = tp + dt;
            positive = positive + dt;
        else
            % ������� ���������� ���������� ������
            fn = fn + dt;            
            negative = negative + dt;
        end
    end

end

i=i+1;

dt = end_time - full_borders(i,1);
if full_borders(i,2) == 1
    auto_started=1;
elseif full_borders(i,2) == 2
    auto_started=0;
elseif full_borders(i,2) == 3
    man_started=1;
elseif full_borders(i,2) == 4
    man_started=0;
end

    
% ����������� ����� �����
if     (man_started==1) && (auto_started==1)
    tp = tp + dt;
    positive = positive + dt;
elseif (man_started==1) && (auto_started==0)
    fn = fn + dt;
    negative = negative + dt;
elseif (man_started==0) && (auto_started==1)
    fp = fp + dt;
    positive = positive + dt;
elseif (man_started==0) && (auto_started==0)
    tn = tn + dt;
    negative = negative + dt;
end

%figure;
R = (manual_mask(1:length(X_),1)~=0).*(voice(1:length(X_),1)==0);
%bar(X_,R.*(-1));
plot(X_,R.*(50)+400,'LineWidth',2);
%hold on
R = (manual_mask(1:length(X_),1)==0).*(voice(1:length(X_),1)~=0);
%bar(X_,R.*(-1));
plot(X_,R().*(50)+500,'LineWidth',2);


fprintf('TP: %5.4f\n',tp/positive);
fprintf('FP: %5.4f\n\n',fp/positive);
fprintf('TN: %5.4f\n',tn/negative);
fprintf('FN: %5.4f\n\n',fn/negative);
