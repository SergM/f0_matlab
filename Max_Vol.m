% ������� ������� ���������
% �� ����� �������� �� N �������� �������
% �� ������ - ������� ������� ��������� ��� ���� ������� � �� (�� ��������� � �����������-����������)

function Vol = Max_Vol(Signal)
    N = size(Signal,1);
    
    Average = max(abs(Signal))/N;
    MaxVal = 1;
    if Average == 0 
        Average = 1/2^15;
    end
    
    Vol = 20*log10(Average/MaxVal);
end