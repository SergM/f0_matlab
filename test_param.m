clear all;
%close all

global Sound Sp F0 Debug NSI thesh_gain Volume F0_Cand voice PRE_VOICE hi_fr_voice mean_noize HNR;
global Vol;

% ����� ������� ������ ���� ����� ���� �������� ������ ����������� ������
% � ��� ����� ���������:
% - ��� ����
% - ������������� (�������������������)
% - ������� F0, F1, F2, F3 ��� ������� ���� ���������
% - ����������� ������-���
% - ������� ���� ��� �������(Noise Strength Index)
% - ���������
% ��������, ��� ���-��

% ��������� �������� ����
% 16000 ��, 16���, ����

Fd = 16000;
% ������ ���� ���� �� ������ ������� 2^n
WSize = 1024;   % ������������ ���� ����� 64 ��

%WSize = 4096;   % ������������ ���� ����� 64 ��
Step = 256;     % ��� ��������� 16��

F0_N_Cand = 8;

thesh_gain = 1;
Last_F0 = 0;

%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0002_129_1.wav';
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0004_131_13.wav';
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0002_129_9.wav';
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0055_199_5.wav';     % 500 200 15
%filename = 'D:\GoogleDisk\�������\������������\Groups\������������\�����\V_000_0060_207_2.wav';     % 500 100 7
%filename = 'E:\F0_Dima\f0_matlab\short\Record_3.wav';

%filename = 'WAV\Interview_1.wav';
%filename = 'WAV\Interview_2.wav';
path = 'WAV\';
name = 'F0_1.wav';
svPath = 'Files';
filename = [path name];
load('Files\F0_1.mat')
F0_test = F0;
%filename = 'WAV\Interview_5.wav';
%filename = 'WAV\������ 6.wav';
%filename = 'WAV\V_000_0073_217.wav';                                                                % 500 100 7
%filename = 'WAV\MicRecord_Tuesday_2_June_2020_19.11.24.wav';
%filename = 'WAV\MicRecord_Tuesday_2_June_2020_19.13.40.wav';

%filename = 'noized1.wav';
%filename = 'noized2.wav';
%filename = 'noized3.wav';
%filename = 'WAV\Noise_100.wav';
%filename = 'WAV\sample.wav';
%filename = 'sample0.wav';
%filename = 'sample1.wav';
%filename = 'sample2.wav';
%filename = 'sample4.wav';
%filename = 'sample5.wav';
%filename = 'sample5_filtred.wav';
%filename = 'sample5_filtred_300-3400.wav';
%filename = 'sample5_N30.wav';
%filename = 'sample5_N100.wav';
%filename = '������ (15).wav';
%filename = '..\..\Audi analitics\Interview_1.wav';
[Sound_,Fs] = audioread(filename);

%Sound_ = filter(1, [1 -0.5], Sound_);
if (size(Sound_,2)==2)
    Sound = resample((Sound_(:,1)+Sound_(:,2))/2,Fd,Fs);
else
    Sound = resample(Sound_(:,1),Fd,Fs);
end

% ����� ���������
Length = size(Sound,1);
%Length = 16000*100;

% ��������� ������������� ������� ��� �����������
YSize = fix(Length/Step); % ����� ������� � ������ ���� ���������
YSize = length(F0);
Sp = zeros(WSize/2, YSize);
Volume = zeros(YSize,1); % ��� ������ ���������

New_Sound = [];

mean_noize =  zeros(YSize,1);

voice = zeros(YSize,1); % ��� VAD
hi_fr_voice = zeros(YSize,1); % ��� ������� VAD

F0 = zeros(YSize,1);
PRE_VOICE = zeros(YSize,1);
F0_Cand = zeros(YSize,F0_N_Cand,6);
ZeroCross = zeros(YSize,1);
NSI = zeros(YSize,1);
Debug = zeros(YSize,6);
HNR = zeros(YSize,1);
Vol = zeros(YSize,1);
Sonogramm = zeros(WSize/2, YSize);  % ��� ����������� ����������
correl = zeros(2, YSize);  % ��� ��������������
spectrogramm = zeros(WSize/2, YSize);
cleansp = zeros(WSize/2, YSize);
LPC = zeros(9, YSize);
formants = zeros(9, YSize);
cccc = zeros(95, YSize);

%N_harm = 5; % ���������� ����������� ��������

fmin = 75;  % ����������� ����������� �������
fmax = 400;  % ������������ ����������� �������

bin_f = Fd/WSize;         % ������� �� ���� ���

Window = Sound(WSize:-1:1,1);
Sound = [Sound; zeros(126*Step,1)];
%WSh = gausswin(WSize,2.5); % ���� ��� �������������� �����
ham = 0.53836 - 0.46164 * cos(2 * 3.1415 * [0:(WSize-1)] / (WSize - 1));
% �������� ����
% ��� ��������� � ���������� ����
% �� ������... ��������, ���-�� � ��������������� ����� ��� ���� �
% �����������������...
i = 1; % ������� ������


period_array = [];


% ��������� VAD
steps_before = 8; % 0.128 ���.
steps_after = 8;  % 0.128 ���.
glue_gap = 8;% 0.256 ���.
extra_before = 2; % 0.032 ���.
extra_after = 2;  % 0.032 ���.

max_back_gap = 63; % 1 ���.
max_prolongation = 126; % 2 ���. (max_back_gap + �������� ��������� ��� F0_Cand)


new_Vad_mask = uint8(zeros(1,1));
new_pointer = 1; %-1
jj=0;
tic

while i <= YSize-WSize/Step
    % �������� ����
    Window = Sound(Step*(i-1)+1:Step*(i-1)+WSize);
    
    % ������� ������ (�� ����) ������
    [Sp(:,i), Ph_sp] = Spectr(Window.*ham');
    
    % ��������� ������� ��������� ����
    F0_cand9(i, Fd, F0_N_Cand); % � ������ ������ ��� �������� ���������

    if (i>max_prolongation) && (i<YSize-WSize/Step-steps_after)
        % ��������� ����� ������ ���� ��� ������������ F0, � ��� �����
        % ���������� ������ ������
        VAD(i-max_prolongation, steps_before, steps_after, max_back_gap, max_prolongation, glue_gap, extra_before, extra_after);
    end

%     if (i>max_prolongation+max_back_gap) && (i<YSize-WSize/Step-steps_after)
%         % VAD ������ ���������� � ����������� � 
%         % ���������� ������ ������
%         F0_trace(i-max_prolongation-max_back_gap, max_back_gap);
%     end
    
    i = i + 1;
    
    if mod(i,1000)==0
        fprintf('���������� %d ����� �� %d\n',i,YSize-WSize/Step);
    end    
end
toc
HNR_slope = 1; % �������� �������� HNR  %�� ������
max_HNR = 5;  % ����� �������� �������� ����� 0 %�� ������
%     HNR_slope = 0.7; % �������� �������� HNR
%     max_HNR = 6;  % ����� �������� �������� ����� 0

%betta = 300; % ����������� ����� HNR ������������ ����   0 - 500 \ 5

FR_slope = 15; % �������� �������� FREQUENCY % �� ������ 
FR_sigm_width = 5;% ������ ����� �������  %�� ������

%gamma = 400; % ����������� ����� ��������� ������������ ����

PWR_slope = 3; % �������� �������� PWR    %�� ������
zcross_PWR = 0.1; % ����� �������� �������� ����� 0 %�� ������
%     PWR_slope = 8; % �������� �������� PWR
%     zcross_PWR = 0.2; % ����� �������� �������� ����� 0

%delta = 50; % ����������� ����� PWR ������������ ����

% fff - 50 |  200
deltaArr = 0;
bettaArr = 500;
gammaArr = 550;
freqWidthArr = 250; % ������� ������ fff
ErrorArr = zeros(length(deltaArr),length(bettaArr),length(gammaArr));%,length(freqWidthArr));
tic 
time1= tic;
count = 1;
allIteration = length(freqWidthArr)*length(deltaArr)*length(bettaArr)*length(gammaArr);

for f = 1:length(freqWidthArr)
%f = 1;
    for d = 1:length(deltaArr)
        for b = 1:length(bettaArr)
            for g = 1:length(gammaArr)
                
                for i = 1 : YSize-WSize/Step % ���� �� ������
                    
                    betta = bettaArr(b); % ����������� ����� HNR ������������ ����   0 - 500 \ 5
                    gamma = gammaArr(g); % ����������� ����� ��������� ������������ ����
                    delta = deltaArr(d); % ����������� ����� PWR ������������ ����
                    freqWidth = freqWidthArr(f);
                    if (i>max_prolongation+max_back_gap) && (i<YSize-WSize/Step-steps_after)
                        % VAD ������ ���������� � ����������� �
                        % ���������� ������ ������
                        
                         F0_trace_1(i-max_prolongation-max_back_gap, max_back_gap,HNR_slope,max_HNR,betta,FR_slope,FR_sigm_width,gamma,PWR_slope,zcross_PWR,delta,freqWidth);
                    end
                    %F0_trace(i, max_back_gap,HNR_slope,max_HNR,betta,FR_slope,FR_sigm_width,gamma,PWR_slope,zcross_PWR,delta,freqWidth);
                    
                end
                
                nonZeroStep = length(F0_test(F0_test ~=0));
%                 acc = zeros(length(F0_test),1);
%                 for z = 1: length(F0_test)
%                     if (F0_test(z) ~= 0)&&(abs(F0(z)-F0_test(z))<5)
%                         acc(z) = 1;
%                     end
%                 end
                
                accAvg = 1 - sum((abs(F0(:)-F0_test(:))<10).*(F0_test(:)~=0))/nonZeroStep;
                sum((abs(F0(:)-F0_test(:))<10).*(F0_test(:)~=0))
                
%                 accAvg = 1 - sum(acc)/nonZeroStep;
                ErrorArr(d,b,g,f) = accAvg;
                F0 = zeros(YSize,1);
                
                time2 = toc;
                TimeStep = (time2- time1);
                timeProc = (TimeStep*(allIteration-count))/60;
                time1 = time2;
                
                if mod(count,10)==0
                    fprintf('���������� %d ����� �� %d, �������� ������� %1.3f �����\n',count,allIteration,timeProc);
                end
                count = count + 1;
            end
        end
    end
end

toc





