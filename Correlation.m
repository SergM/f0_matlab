global Sound LP_ERR;

% ��������� �������� ����
% 16000 ��, 16���, ����
filename = 'Voice_1_2_16k.wav';
[Sound_,Fs] = audioread(filename);

% ����� ���������
%Length = size(Sound_,1);
Length = 16000*5;


Fd = 16000;
min_F = 80;
max_F = 700;
min_per = round(Fd/max_F); 
max_per = round(Fd/min_F);


% ����� ���� � ��������� 

% ��������� ������������� ������� ��� �����������
YSize = fix(Length/Step); % ����� ������� � ������ ���� ���������

LP_ERR = zeros(Length,1);
LPC = zeros(Length,1);
new_LP_ERR = zeros(Length,1);
CORR = zeros(Length,1);

new_Win = zeros(WSize,1);
win1 = zeros(WSize+Step,1);
win2 = zeros(WSize+Step,1);

Sound = filter(1, [1 -0.63], Sound_);
%Sound = Sound_;


% �������� ����
% ��� ��������� � ���������� ����
i = 0;
tic
while i <= Y_Size-2
    % �������� ����
    Window = Sound(Step*i+1:Step*i+WSize,1);
        
    % �������� ������������ �������� LPC
    Coeff = (lpc(Window,LPC_Order))';
    
    win1(1+quart_win:WSize/2+quart_win) = new_Win(1+quart_win:WSize/2+quart_win);
    
    % ������� ������ ������������
    new_Win = LPC_Filter(Window,Coeff);
    
    win2(1+quart_win+Step:WSize/2+quart_win+Step) = new_Win(1+quart_win:WSize/2+quart_win);
    
    LP_ERR(Step*i+1+quart_win:Step*i+WSize/2+quart_win) = new_Win(1+quart_win:WSize/2+quart_win);
    
    LPC(Step*i+1+quart_win:Step*i+WSize/2+quart_win) = Window(1+quart_win:WSize/2+quart_win)-new_Win(1+quart_win:WSize/2+quart_win);
    
    % ���������� ���� ��������� ����� �������������� ��� ��������� ��������
    % ��������� ��������� ���������
    
%     Corr_win = new_Win(1+quart_win:WSize/2+quart_win);
%     for j = -max_per:max_per
%         CORR(i*Step+WSize/2+1+j) = sum(new_Win(Step*i+1+quart_win+j:Step*i+WSize/2+quart_win+j).*Corr_win,1);
%     end
 
    i = i + 1;
end
toc

% �������

X  = 0:1/Fd:(Length-1)/Fd; % ����� ��� ����������� ������� �������
End_of_time = (Length-1)/Fd;

figure('Name',filename);

x_lim = [1.55 1.65];

% ���������� ����
subplot(2,1,1);
plot(X,LPC(1:Length));
hold on
plot(X,Sound(1:Length));
title('������������','FontName','Arial','FontSize',10)
xlim(x_lim);

%hold on;

subplot(2,1,2);
plot(X,LP_ERR);
hold on;

% ��� ������ ��������� ������������ ��������� (����� ��������)
% for i = (1 + min_per):(Length-min_per)
%     center = LP_ERR(i);
%     accum = 0;
%     for j = -min_per:min_per
%         accum = accum + (LP_ERR(i+j)<center);
%     end
%     new_LP_ERR(i) = (accum >= (min_per*2));
% end

plot(X,new_LP_ERR.*LP_ERR);

title('������ ��������������������','FontName','Arial','FontSize',10)
xlim(x_lim);
%ylim([-20 40]);



