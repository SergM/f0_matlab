function F0_cand8(pntr, Fd, fmin, fmax, N_candidates)

    global Sp F0 HNR;
    global SNR;
    
    %   Sp - ����������� ������������������� ������ �� ����
    %   Fd - ������� �������������
    %   fmin - ����������� ����������� ������� ��������� ���� (���)
    %   fmax - ������������ ����������� ���
    %   N_candidates - ���������� ����������� ���������� ���

    % ���������� ���������� (�� ����� ������� � ��������)
    good_HNR = 7;
    bad_HNR = 1.3; %<= ���� 1.5
    
    %good_peak_HNR = 7;
    bad_peak_HNR = 3; %<= ���� 3
   
    check_harm = 5;

    N = size(Sp,1);   % ���������� ����� � �������������� �������
    bin_f = Fd/2/N;   % ������� �� ���� ���
    
    % ������ � ��������� ���������� � ���
    Candidates = zeros(N_candidates,8);% 1-� - ���������-��� � ������
                                       % 2-� - ������� ��������� ���� (���)
                                       % 3-� - ������� �������� � ������
                                       % 4-� - ��� � ������
                                       % 5-� - ������
                                       % 6-� - ������� ������ ��������� ���
                                       % 7-� - ���������� �� ��������� ������������ ���
    
    
%     % ��� �������     
    ccc = []; % ���������� ������
    ccc1 = []; % ���������� ������
    fff = []; % ���������� ������
    
    
    % ������� ����� ������������� ���� ������� � ��������� �� 1500��
    [max_pwr,max_pwr_bin] = max(Sp(1:round(1500/bin_f),pntr));
    
    if max_pwr > 0
        Dithering_factor = max_pwr/1000;
    else
        Dithering_factor = 1E-6;
    end
    
    
    % �� ������� ������� ����
    % ����� ������� ���, ������ �������� ���� ��� ����� ������� ������
    %peaks_length = round(fmax*check_harm/bin_f)+1;
    %if peaks_length>length(Sp(:,pntr))
        peaks_length=length(Sp(:,pntr));
    %end
    peaks = zeros(peaks_length,1);
    peaks_start = round(fmin/bin_f)-1;
    for i=peaks_start:peaks_length-2
        if (Sp(i-1,pntr)<=Sp(i,pntr)) && (Sp(i,pntr)>=Sp(i+1,pntr))% && (Sp(i-2,pntr)<=Sp(i,pntr)) && (Sp(i,pntr)>=Sp(i+2,pntr))
            peaks(i) = Sp(i,pntr);
            peaks(i+1) = Sp(i+1,pntr);
            peaks(i-1) = Sp(i-1,pntr);
        else
            if peaks(i)==0
                peaks(i)=Dithering_factor;
            end
        end
    end

    f = fmin;
    VocalHNR = 10^100;
    HNR_to_store = 0;
    
    % �������� ���������� �� ��� � ��������� ���������� �������� � ���
    while f<fmax
         
        % ��������� ��� ���� ��� ����������� ���������-��� �� ������
        % check_harm ����������
        harmonics = 0;
        SpHarmonics = 0;

        % ���������� ������� ��� ���� � ������� � ������� ����
        
        % ������� ������ ��� ��������� �������
        first_bin = round(f/bin_f)+1;
        % ������� ��������� � ������������� ���� ������� ��� � ���������
        rate = round(max_pwr_bin/first_bin);
        
        if (f < 70) && rate==1
            rate = 2;
        end
        
        %  �������� ������ ����������� ��������� ���, ��� �� ������������
        %  ��� ������� �� �������� ��������� ����������� �������� ���� ���
        %  ��������
        mid_check_band = round((check_harm+1)/2);
        
        if rate >= mid_check_band
            start_c = rate - mid_check_band + 1;
        else
            start_c = 1;
        end
        
        % ��������� ��������� ��������� ���� 60�� ��� ���������� ����
        % ����������� ����
        start_c = start_c + fix((100/(start_c*f)));
        
        
        % ���������� check_harm �������� ������� �� start_c
        
        for c = start_c:(start_c + check_harm - 1)
            current_bin = round(f*c/bin_f)+1;
            if current_bin > 3%6
                SpHarmonics = SpHarmonics + Sp(current_bin,pntr)+...
                    Sp(current_bin+1,pntr)+Sp(current_bin-1,pntr);
            end
        end
%         
        for c = start_c:(start_c + check_harm - 1)
            current_bin = round(f*c/bin_f)+1;
            if current_bin > 3%6
                
                harmonics = harmonics + peaks(current_bin)+...
                    peaks(current_bin+1)+peaks(current_bin-1);
                
            end
        end
        % ��� - ��� �� ��������� (��������� �� ���������� �����)
        noize = (sum(peaks(5:current_bin+1))-harmonics)/...
            ((current_bin)-3*check_harm-5);
        
        SpNoize = (sum(Sp(5:current_bin+1,pntr))-SpHarmonics)/...
            ((current_bin)-3*check_harm-5);        
        
        % ������ ���� ��������� �� ���������� �����
        harmonics = harmonics/(3*check_harm);
        prev_VocalHNR = VocalHNR;
        VocalHNR = (harmonics/noize);
        
        % ������ ���� ��������� �� ���������� �����
        SpHarmonics = SpHarmonics/(3*check_harm);
        SpVocalHNR = ((SpHarmonics+Dithering_factor)/(SpNoize+Dithering_factor));
        
        if HNR_to_store<SpVocalHNR
            HNR_to_store = SpVocalHNR;
        end
        
%         % ��� �������
        fff = [fff f];
        ccc = [ccc SpVocalHNR];
        ccc1 = [ccc1 VocalHNR];


        insert_pos = 0;
        lowest_level = 10^100;
        if (VocalHNR > bad_peak_HNR) && (VocalHNR > prev_VocalHNR) && (SpVocalHNR > bad_HNR)
            for i = 1:N_candidates
                % ���������� ���� ����������
                % ���� ������� ������, �� �������� ������� � SNR
                if (f - Candidates(i,2)) <= bin_f*2
                    % ���� ������� ����, �� ��������
                    if (VocalHNR >= Candidates(i,3))
                        Candidates(i,1) = SpVocalHNR;
                        Candidates(i,2) = f;
                        Candidates(i,3) = VocalHNR;
                        Candidates(i,4) = SpNoize;
                        Candidates(i,8) = start_c;
                        insert_pos = 0;
                        break
                    end
                else
                    if (VocalHNR >= Candidates(i,3))
                        % ���������� ����������� ������� ��������� � ���
                        % �������
                        if Candidates(i,1) < lowest_level
                            lowest_level = Candidates(i,1);
                            insert_pos = i;
                            if Candidates(i,1) == 0
                                % ���� ������ ���, �� ����� ������ �� ����������
                                break
                            end
                        end
                    end
                end
            end    
            % ���� ����� ����� ��� � �� ��������
            if insert_pos ~= 0
                % �� �� � �� ���� ���������, ���� ����� ���� ����� �������
                % �����
                if insert_pos > 1
                    prev_cand_freq = Candidates(insert_pos-1,2);
                else
                    prev_cand_freq = 0;
                end
                if ((f - prev_cand_freq) > bin_f*2)
                    Candidates(insert_pos,1) = SpVocalHNR;
                    Candidates(insert_pos,2) = f;
                    Candidates(insert_pos,3) = VocalHNR;
                    Candidates(insert_pos,4) = SpNoize;
                    Candidates(insert_pos,8) = start_c;
                end
            end
        end
        f=f+bin_f/check_harm;
    end
    
    % ������� ��� ����������
    current_noize = 1;
    for i = 1:N_candidates
        if (Candidates(i,4)~=0) && (Candidates(i,4) < current_noize)
            current_noize = Candidates(i,4);
        end
    end
    for k = 1:N_candidates
        % ��������� ��������� �������� ��������� �� ���� �������
        % current_noize = Candidates(k,4);
        min_harm_pwr = 1;
        max_harm_pwr = 0;
        for j=Candidates(k,8):Candidates(k,8)+2
            harm_pwr = Sp(round(Candidates(k,2)*j/bin_f)+1,pntr);
            if harm_pwr > max_harm_pwr
                max_harm_pwr = harm_pwr;
            end
            if harm_pwr < min_harm_pwr
                min_harm_pwr = harm_pwr;
            end
            if current_noize*bad_HNR >= harm_pwr
                Candidates(k,5) = 1;
                %Candidates(k,6) = harm_pwr;
                %break
%              else
%                if harm_pwr>Candidates(k,6)
%                    Candidates(k,6)=harm_pwr;
%                end
            end
        end
        Candidates(k,6)=max_harm_pwr;
%         if min_harm_pwr/max_harm_pwr < 0.3
%             Candidates(k,5) = 1;
%             %Candidates(k,6)=min_harm_pwr;
%         end
    end

    
%     % ��� �������
    if pntr == 2053
        figure('Name','Debug');
        subplot(2,1,1);
        plot(fff,ccc,'LineStyle','-','LineWidth',2);
        hold on;
        plot(fff,ccc1,'LineStyle','-','LineWidth',1);
        plot(Candidates(:,2),Candidates(:,1),'LineStyle','none','LineWidth',2,'Marker','o');
        plot(Candidates(:,2),Candidates(:,6).*100,'LineStyle','none','LineWidth',2,'Marker','x');
        xlim([0 2*fmax]);
        subplot(2,1,2);
        bar(0:bin_f:Fd/2-bin_f,Sp(:,pntr));
        hold on;
        bar(0:bin_f:(length(peaks)-1)*bin_f,peaks(:));
        xlim([0 2*fmax]);
    end
    
    % ���� ��������� ��������� ������� ��������� ����
    k = pntr;
    last_nz_fr = 0;
    last_nz_fr_lvl = max_pwr;
    % ���� �� ������ 64 ����� (64*256/16000 = 1,024�)
    back_lookup = 64;
    if pntr > back_lookup
        do_count = 0;
        while k > pntr - back_lookup + 3
            k = k - 1;
            near_fr_threshold = 2^(k-pntr);
            if ((F0(k)~=0) && (F0(k-1)~=0)) || do_count==1
                do_count = 1;
                % ������� ��������� ������� �� 4 ���� ������� � ������
                % ���������
                last_nz_fr = sum(F0(k-3:k))/sum(F0(k-3:k)>0);
                % ��������� ����� �������
                delta = 0;
                for i=k-2:k
                    if F0(i-1)~=0 && F0(i)~=0
                        delta = delta + (F0(i)-F0(i-1));
                    end
                end
                last_nz_fr = last_nz_fr + delta;
                % �������� ������� ������� �� ������� ��������� ���������
                % ��� 4-� ����� ����� ������� � ��������� ���������
                last_nz_fr_lvl = mean(Sp(round(f/bin_f)+1,k-3:k));
                break;
            end
        end
    end
    
    
    % ��������� ���������� �� ��������� ��������� ���
    % � ������� ������������ ������� HNR
    max_peak_HNR = 0;
    max_HNR = 0;
    max_peak_HNR_second = 0;
    max_HNR_second = 0;
    ind = 0;
    ind_peak = 0;
    ind_near_fr = 0;
    min_fr_threshold = 2;
    for k = 1:N_candidates
        if last_nz_fr~=0
            Candidates(k,7) = abs(last_nz_fr - Candidates(k,2))/last_nz_fr;
             if Candidates(k,7) < near_fr_threshold
                 Candidates(k,5) = 0;
                 % ���� ����� ������� � ���������� ��� ��������.
                 if Candidates(k,7) < min_fr_threshold
                    min_fr_threshold = Candidates(k,7);
                    ind_near_fr = k;
                 end
             end
        else
            Candidates(k,7) = 1000;
        end
        if (Candidates(k,5) ~= 1) && (Candidates(k,3) > max_peak_HNR)
            max_peak_HNR_second = max_peak_HNR;
            max_peak_HNR = Candidates(k,3);
            ind_peak = k;
        end
        if (Candidates(k,5) ~= 1) && (Candidates(k,1) > max_HNR)
            max_HNR = Candidates(k,1);
            ind = k;
        end
    end

    for k = 1:N_candidates
        if (k~=ind_peak) && (Candidates(k,5) ~= 1) && (Candidates(k,3) > max_peak_HNR_second)
            max_peak_HNR_second = Candidates(k,3);
        end
        if (k~=ind) && (Candidates(k,5) ~= 1) && (Candidates(k,1) > max_HNR_second)
            max_HNR_second = Candidates(k,1);
        end
    end
    
    not_sure = 0;
    if ((max_HNR - max_HNR_second)/max_HNR < 0.10)
        not_sure = 1;
    end

    not_sure_peak = 0;
    if ((max_peak_HNR - max_peak_HNR_second)/max_peak_HNR < 0.10)
        not_sure_peak = 1;
    end
    if (not_sure == 1) && (not_sure_peak == 0)
        ind = ind_peak;
    end
    
    % �� ��������� ��� ���, ��� ������������ HNR
    if ind == 0
        F0_selected = 0;
    else
        F0_selected = Candidates(ind,2);
    end
    
    
    
    % ���� HNR ���� ������������ ������, �� ��� = 0
    if (max_HNR < bad_HNR)
    
        F0_selected = 0;
    
    elseif ((max_HNR >= bad_HNR) && (max_HNR <= good_HNR)) 

        if (ind_near_fr~=0) && (Candidates(ind_near_fr,1) > bad_HNR)
            ind = ind_near_fr;
            F0_selected = Candidates(ind_near_fr,2);
        elseif not_sure && not_sure_peak
        % ��������� ������� �������� HNR �� �����
        % �� �� �������� � ������, ����� �������� ��������� �� �������
        % ���������� ��������� ���
            if last_nz_fr ~= 0
                % ���������� ���� ��������������� ���������� � ��������
                % ��������� � ����������� ����������� �� ���������� ���
                min_fr_diff = 1000;
                for i = 1:N_candidates
                    if min_fr_diff > Candidates(i,7)
                        min_fr_diff = Candidates(i,7);
                        ind = i;
                    end
                    if (min_fr_diff < 0.1)
                        % ���� ��� ���������� �� ����� ��� �� 130% 
                        F0_selected = Candidates(ind,2);
                        break;
                    else
                        % ������ ����� ��� �� ���� ����������� ���������
                        F0_selected = 0;
                    end
                end
    %             else
    %                  % ���� �� ������� ���������� ���, �� ��������� �� �� ���
    %                  % � ������� �� �����������
    %                 F0_selected = 0;
    %             end
            end
        end
        if Candidates(ind,6) < last_nz_fr_lvl*0.5
            F0_selected = 0;
        end
    end
        
    if pntr > 2
        if abs(F0_selected - F0(pntr-2)) <= 0.1*F0_selected
            F0_2_mid = (F0_selected + F0(pntr-2))/2;
            if abs(F0(pntr-1)-F0_2_mid) > (F0_2_mid/1.05)
                F0(pntr-1) = F0_2_mid;
            end
        end
    end
    
    if F0_selected == 0
%         [HNR(pntr), ind] = max(Candidates(:,1));
%         if Candidates(ind,5) == 1
%             HNR(pntr) = 0;
%         end
        HNR(pntr) = HNR_to_store;
    else
        HNR(pntr) = Candidates(ind,1);
    end
    
    %[~,ind] = sort(Candidates(:,3),1);
    %F0_selected = Candidates(ind(N_candidates),2);
    
%    F0_Cand(pntr,:) = Candidates(:,2).*(Candidates(:,5)==0);
    SNR(pntr,:) = Candidates(:,1);
    F0(pntr) = F0_selected;
%    NSI(pntr,:) = Candidates(:,5);
    
end