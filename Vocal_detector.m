% �������� �������������� ��������
% �� ����� ��������������� ������ �����
function voc = Vocal_detector(Frame,Volume,Spectr)
    CrossingThreshold = 200;
    VolThreshold = -45;
    FlatnesMeasure = -2;

    N = size(Spectr,1)/8;
    L = size(Frame,1);
    
    % ������� ���������� ��������� ����� 0
    Signs = int8(sign(Frame)>0);
    ZeroCross = sum(abs([0;Signs(1:L-1)]-[Signs(1:L-1);0]));
    
    % �������� ���� ��������� ������� (�������� �������, �� ��������)
    Am = sum(Spectr(1:N))/N;
    GG=1;
    for i=1:N
        GG = GG*Spectr(i,1);
    end
    Gm = GG^(1/N);
    
    %voc = 10*log10(Gm/Am);
    voc = int8((ZeroCross<CrossingThreshold) && (Volume>VolThreshold) && (10*log10(Gm/Am)<FlatnesMeasure));
end