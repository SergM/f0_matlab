% �������� �������������� ��������
% �� ����� ��������������� ������ �����
function ZeroCross = Zero_Cross(Frame)

    L = size(Frame,1);
    
    % ������� ���������� ��������� ����� 0
    Signs = int8(sign(Frame)>0);
    ZeroCross = sum(abs([0;Signs(1:L-1)]-[Signs(1:L-1);0]));
    
end