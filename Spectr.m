 function [Spectr, Ph] = Spectr(Window)
    L = size(Window,1);
    Full = fft(Window);
    P2 = abs(Full/L);
    %Ph = atan(imag(Full(1:L/2))./real(Full(1:L/2)));
    Ph = 0;
    Spectr = [P2(1);2*P2(2:L/2)]+0.000001;
end