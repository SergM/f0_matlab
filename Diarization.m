
clear all;

% ��������� ���� ������
%filename = 'D:\YandexDisk\Speach\������\������ (15).mp3';

filename = 'F:\2020-02-17_Audio_Transfer\V_000_0013_138.wav';

[Sound,Fs] = audioread(filename);

L = size(Sound,1);

Fd = 16000;

Win_length = 0.64;
Win_size = Fs * Win_length; % ���� � 0,64�

new_L = round(L/Fs*Fd);
Step = 256;

Diar = zeros(round(new_L/Step),1);

step_num = Win_length/(Step/Fd);

stereo_hysteresis = 0.15;
stereo_trigger = 0;

Dict = zeros(ceil(L/Win_size),1);
Dict2 = zeros(ceil(L/Win_size),1);
Peak = 0;
N=8;
Avg=zeros(8,1);

w = 1;
% ��� ������� ������ �������� ������� �������� � ����
for i=1:Win_size:L-Win_size
    
    % ������� ��������� � ���������� ���� ��� ������� ������
    Max_Left = max(abs(Sound(i:i+Win_size-1,1)));
    Max_Right = max(abs(Sound(i:i+Win_size-1,2)));

    Peak = max([Max_Left Max_Right]);

    
    Criterion = (Max_Left - Max_Right)/Peak;
    if w>N
        Avg(2:end) = Avg(1:end-1);
        Avg(1) = Criterion;
    end
    
    Dict2(w) = Criterion;
    Dict(w) = sum(Avg)/N;
    
    if Criterion > stereo_hysteresis
        stereo_trigger = 1;
    elseif Criterion < -stereo_hysteresis
        stereo_trigger = 0;
    end
    
    for j = 0:step_num-1
        Diar(((w-1)*step_num)+j+1) = stereo_trigger;
    end
    
    w=w+1;
end


bar([Win_size/Fs/2:Win_size/Fs:L/Fs-Win_size/Fs+Win_size/Fs+Win_size/Fs/2],Dict2);
hold on;
plot([0:1/Fs:L/Fs-1/Fs],Sound(:,1));
plot([0:1/Fs:L/Fs-1/Fs],Sound(:,2));
plot([0:Win_size/Fs:L/Fs-Win_size/Fs+Win_size/Fs],ones(length(Dict2),1).*stereo_hysteresis);
plot([0:Win_size/Fs:L/Fs-Win_size/Fs+Win_size/Fs],ones(length(Dict2),1).*(-stereo_hysteresis));
plot([Step/Fd/2:Step/Fd:new_L/Fd-Step/Fd+Step/Fd],Diar-0.5,'LineWidth',2);


ylim([-0.7 0.7]);
xlim([0 100]);