function ir = IIR_IR(Coeff, N)
    Order = size(Coeff,1);
    % ������� ���� ��������� � ������ 00000...01 ������, ������ �����
    % ������� �������������
    window = zeros(N+Order,1);
    window(Order) = 1;
    
    for i = 1:N
        accum = 0;
        for j=2:Order
            accum = accum + window(Order+i-j)*Coeff(j);
        end
        window(i+Order-1) = 1*window(i+Order-1)-accum;
    end
    ir = window(Order:Order+N-1);
    
end